package ru.cft.shapes.builder;

import ru.cft.shapes.product.Shape;

import java.util.List;

public interface ShapeBuilder {

    Shape build(List<Double> parameters);
}
