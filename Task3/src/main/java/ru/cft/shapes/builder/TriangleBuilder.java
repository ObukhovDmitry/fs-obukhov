package ru.cft.shapes.builder;

import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Triangle;

import java.util.List;

public class TriangleBuilder implements ShapeBuilder {

    public Triangle build(List<Double> parameters) {

        if (!checkParameters(parameters)) {
            throw new IncorrectShapeParametersException();
        }

        return new Triangle(parameters.get(0), parameters.get(1), parameters.get(2));
    }

    private boolean checkParameters(List<Double> parameters) {

        if (parameters.size() != 3) {
            return false;
        }

        double a = parameters.get(0);
        double b = parameters.get(1);
        double c = parameters.get(2);

        return a > 0 && a < b + c &&
                b > 0 && b < a + c &&
                c > 0 && c < a + b;
    }
}
