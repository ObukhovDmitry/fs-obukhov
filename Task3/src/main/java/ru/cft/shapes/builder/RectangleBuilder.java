package ru.cft.shapes.builder;

import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Rectangle;

import java.util.List;

public class RectangleBuilder implements ShapeBuilder {

    public Rectangle build(List<Double> parameters) {

        if (!checkParameters(parameters)) {
            throw new IncorrectShapeParametersException();
        }

        return new Rectangle(parameters.get(0), parameters.get(1));
    }

    private boolean checkParameters(List<Double> parameters) {
        return parameters.size() == 2 && parameters.get(0) > 0 && parameters.get(1) > 0;
    }
}
