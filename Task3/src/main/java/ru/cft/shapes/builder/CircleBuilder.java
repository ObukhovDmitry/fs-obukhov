package ru.cft.shapes.builder;

import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Circle;

import java.util.List;

public class CircleBuilder implements ShapeBuilder {

    public Circle build(List<Double> parameters) {

        if (!checkParameters(parameters)) {
            throw new IncorrectShapeParametersException();
        }

        return new Circle(parameters.get(0));
    }

    private boolean checkParameters(List<Double> parameters) {
        return parameters.size() == 1 && parameters.get(0) > 0;
    }
}
