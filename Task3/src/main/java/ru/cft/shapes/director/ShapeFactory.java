package ru.cft.shapes.director;

import ru.cft.shapes.IncorrectShapeNameException;
import ru.cft.shapes.builder.CircleBuilder;
import ru.cft.shapes.builder.RectangleBuilder;
import ru.cft.shapes.builder.ShapeBuilder;
import ru.cft.shapes.builder.TriangleBuilder;
import ru.cft.shapes.product.Shape;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShapeFactory {

    private static final String CIRCLE_NAME = "CIRCLE";
    private static final String RECTANGLE_NAME = "RECTANGLE";
    private static final String TRIANGLE_NAME = "TRIANGLE";

    private Map<String, ShapeBuilder> builders;

    public ShapeFactory() {

        builders = new HashMap<>();

        builders.put(CIRCLE_NAME, new CircleBuilder());
        builders.put(RECTANGLE_NAME, new RectangleBuilder());
        builders.put(TRIANGLE_NAME, new TriangleBuilder());
    }

    public Shape getShape(String shapeName, List<Double> parameters) {

        if (!checkName(shapeName)) {
            throw new IncorrectShapeNameException();
        }

        return builders.get(shapeName).build(parameters);
    }

    private boolean checkName(String shapeName) {
        return builders.containsKey(shapeName);
    }
}
