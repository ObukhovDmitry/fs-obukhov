package ru.cft.shapes.product;

import java.util.Arrays;
import java.util.Locale;

public class Triangle extends Shape {

    private static final String TRIANGLE_RU_NAME = "Треугольник";
    private static final int NUMBER_OF_SIDES = 3;
    private double[] sides;

    public Triangle(double a, double b, double c) {

        super(TRIANGLE_RU_NAME);
        sides = new double[] {a, b, c};
    }


    public double[] getSides() {
        return sides;
    }

    public double[] getAngles() {

        // Теорема косинусов

        double[] angles = new double[NUMBER_OF_SIDES];
        double[] sidesSquare = new double[] {
                sides[0] * sides[0],
                sides[1] * sides[1],
                sides[2] * sides[2]};

        for (int i = 0; i < NUMBER_OF_SIDES; i++) {
            int j = (i + 1) % NUMBER_OF_SIDES;
            int k = (i + 2) % NUMBER_OF_SIDES;

            double cos = (sidesSquare[j] + sidesSquare[k] - sidesSquare[i]) /
                    (2.0 * sides[j] * sides[k]);
            double anglePI = Math.acos(cos);
            angles[i] = 180.0 * anglePI / Math.PI;
        }

        return angles;
    }

    @Override
    public double getArea() {

        // Формула Герона

        double p = (sides[0] + sides[1] + sides[2]) / 2.0;
        return Math.sqrt(p * (p - sides[0]) * (p - sides[1]) * (p - sides[2]));
    }

    @Override
    public double getPerimeter() {
        return sides[0] + sides[1] + sides[2];
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Triangle triangle = (Triangle) o;

        return Arrays.equals(sides, triangle.sides);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(sides);
    }

    @Override
    public String toString() {

        double[] angles = getAngles();

        char deg = '\u00b0';
        String format = super.toString() +
                "\nСторона 1: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT +
                "\nСторона 2: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT +
                "\nСторона 3: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT +
                "\nУгол противолежащий к стороне 1: %." + PRINT_PRECISION + "f" + deg +
                "\nУгол противолежащий к стороне 2: %." + PRINT_PRECISION + "f" + deg +
                "\nУгол противолежащий к стороне 3: %." + PRINT_PRECISION + "f" + deg;

        return String.format(Locale.US, format, sides[0], sides[1], sides[2], angles[0], angles[1], angles[2]);
    }
}
