package ru.cft.shapes.product;

import java.util.Locale;

public class Rectangle extends Shape {

    private static final String RECTANGLE_RU_NAME = "Прямоугольник";
    private double length;
    private double width;

    public Rectangle(double length, double width) {

        super(RECTANGLE_RU_NAME);

        this.length = Math.max(length, width);
        this.width = Math.min(length, width);
    }

    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }

    public double getDiagonal() {
        return Math.sqrt(length * length + width * width);
    }

    @Override
    public double getArea() {
        return length * width;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * (length + width);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Rectangle rectangle = (Rectangle) o;

        return Double.compare(length, rectangle.length) == 0 &&
                Double.compare(width, rectangle.width) == 0;
    }

    @Override
    public int hashCode() {
        return 31 * Double.hashCode(length) + Double.hashCode(width);
    }

    @Override
    public String toString() {

        String format = super.toString() +
                "\nДлина диагонали: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT +
                "\nДлина: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT+
                "\nШирина: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT;

        return String.format(Locale.US, format, getDiagonal(), getLength(), getWidth());
    }
}
