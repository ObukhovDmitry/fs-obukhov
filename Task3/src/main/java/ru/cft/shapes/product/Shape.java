package ru.cft.shapes.product;

import java.util.Locale;

public abstract class Shape {

    protected static final int PRINT_PRECISION = 3;
    protected static final String MEASUREMENT_UNIT = "м";
    private String name;

    public Shape(String name) {
        this.name = name;
    }

    public abstract double getArea();
    public abstract double getPerimeter();

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shape shape = (Shape) o;

        return name != null ? name.equals(shape.name) : shape.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {

        String format = "Тип фигуры: %s" +
                "\nПлощадь: %." + PRINT_PRECISION + "f кв." + MEASUREMENT_UNIT +
                "\nПериметр: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT;

        return String.format(Locale.US, format, name, getArea(), getPerimeter());
    }
}
