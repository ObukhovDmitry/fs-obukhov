package ru.cft.shapes.product;

import java.util.Locale;

public class Circle extends Shape {

    private static final String CIRCLE_RU_NAME = "Круг";
    private double radius;

    public Circle(double radius) {

        super(CIRCLE_RU_NAME);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getDiameter() {
        return 2.0 * radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * Math.PI * radius;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Circle circle = (Circle) o;

        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(radius);
    }

    @Override
    public String toString() {

        String format = super.toString() +
                "\nРадиус: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT +
                "\nДиаметр: %." + PRINT_PRECISION + "f " + MEASUREMENT_UNIT;

        return String.format(Locale.US, format, getRadius(), getDiameter());
    }
}
