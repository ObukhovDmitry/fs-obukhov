package ru.cft.shapeio;

public class ShapeReaderException extends RuntimeException {
    public ShapeReaderException(Exception e) {
        super(e);
    }
}
