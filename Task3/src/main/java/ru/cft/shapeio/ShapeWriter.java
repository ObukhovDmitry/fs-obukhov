package ru.cft.shapeio;

import ru.cft.io.OutputStream;
import ru.cft.shapes.product.Shape;

public class ShapeWriter {

    private OutputStream output;

    public ShapeWriter(OutputStream output) {
        this.output = output;
    }

    public void print(Shape shape) {
        output.print(shape.toString());
    }
}
