package ru.cft.shapeio;

import ru.cft.io.InputStream;
import ru.cft.shapes.IncorrectShapeNameException;
import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Shape;
import ru.cft.shapes.director.ShapeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ShapeReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShapeReader.class);
    private InputStream input;
    private ShapeFactory shapeFactory;

    public ShapeReader(InputStream input) {

        shapeFactory = new ShapeFactory();
        this.input = input;
    }

    public Shape read() {

        LOGGER.debug("Чтение фигуры началось.");
        try {
            String shapeName = readShapeName();
            LOGGER.debug("Имя фигуры считано успешно.");

            List<Double> parameters = readParameters();
            LOGGER.debug("Параметры фигуры считано успешно в количестве {}.", parameters.size());

            return shapeFactory.getShape(shapeName, parameters);

        } catch (IncorrectShapeNameException e) {

            LOGGER.debug("Ошибка. Указано некорректное имя фигуры.");
            throw e;

        }  catch (IncorrectShapeParametersException e) {

            LOGGER.debug("Ошибка. Указаны некорректные параметры фигуры.");
            throw e;

        } catch (Exception e) {
            throw new ShapeReaderException(e);
        }
    }

    private String readShapeName() {
        return input.getNextString();
    }

    private List<Double> readParameters() {

        List<Double> parameters = new ArrayList<>();

        while (input.hasNextDouble()) {
             parameters.add(input.getNextDouble());
        }

        return parameters;
    }
}
