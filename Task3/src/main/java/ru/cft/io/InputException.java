package ru.cft.io;

public class InputException extends RuntimeException {
    public InputException(Exception e) {
        super(e);
    }
}
