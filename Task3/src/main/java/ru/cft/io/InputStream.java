package ru.cft.io;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

public class InputStream implements AutoCloseable {

    private static final String CHARSET = "UTF-8";
    private Scanner in;

    public InputStream() {}

    public InputStream(String filename) {

        try {
            in = new Scanner(Paths.get(filename), CHARSET);
            in.useLocale(Locale.US);
        } catch (IOException e) {
            throw new InputException(e);
        }
    }

    public String getNextString() {
        return in.nextLine();
    }

    public double getNextDouble() {
        return in.nextDouble();
    }

    public boolean hasNextDouble() {
        return in.hasNextDouble();
    }

    @Override
    public void close() {
        in.close();
    }
}
