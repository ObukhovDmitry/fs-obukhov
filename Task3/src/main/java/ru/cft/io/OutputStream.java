package ru.cft.io;

import java.io.IOException;
import java.io.PrintWriter;

public class OutputStream  implements AutoCloseable {

    private PrintWriter writer;

    public OutputStream() {}

    public OutputStream(String[] args) {

        if (args.length == 0) {
            initConsole();
        }
        if (args.length == 1) {
            initFile(args[0]);
        }
    }

    public void print(String message) {
        writer.print(message);
    }

    private void initConsole() {
        writer = new PrintWriter(System.out);
    }

    private void initFile(String filename) {

        try {
            writer = new PrintWriter(filename);
        } catch (IOException e) {
            throw new OutputException(e);
        }
    }

    @Override
    public void close() {
        writer.close();
    }
}
