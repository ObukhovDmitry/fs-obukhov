package ru.cft.io;

public class OutputException extends RuntimeException {
    public OutputException (Exception e) {
        super(e);
    }
}
