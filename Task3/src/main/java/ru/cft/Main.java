package ru.cft;

import ru.cft.io.*;
import ru.cft.shapeio.ShapeReaderException;
import ru.cft.shapeio.ShapeReader;
import ru.cft.shapeio.ShapeWriter;
import ru.cft.shapes.IncorrectShapeNameException;
import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Shape;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private static final String FLAG_CONSOLE = "-c";
    private static final String FLAG_FILE = "-f";
    private static String outputStreamName;


    public static void main(String[] args) {

        if (!checkArgs(args)) {

            LOGGER.error("Параметры указаны не корректно.");
            return;
        }

        try (InputStream input = new InputStream(args[0]);
             OutputStream output = new OutputStream(getOutputArgs(args))) {

            ShapeReader shapeReader = new ShapeReader(input);
            Shape shape = shapeReader.read();

            LOGGER.info("Чтение фигуры произведено.");

            ShapeWriter shapeWriter = new ShapeWriter(output);
            shapeWriter.print(shape);

            LOGGER.info("Запись фигуры в {} выполнена.", outputStreamName);

        } catch (IncorrectShapeNameException e) {

            LOGGER.error("Имя фигуры в файле указано некорректно.");

        } catch (IncorrectShapeParametersException e) {

            LOGGER.error("Параметры фигуры в файле указаны некорректно.");

        } catch (ShapeReaderException e) {

            LOGGER.error("Невозможно прочитать входные данные из файла. Возникла проблема при чтении входных данных.");

        } catch (InputException e) {

            LOGGER.error("Невозможно прочитать входные данные из файла. Проблема с доступом к файлу.");

        } catch (OutputException e) {

            LOGGER.error("Невозможно записать данные в файл. Проблема с доступом к файлу.");
        }
    }

    private static boolean checkArgs(String[] args) {

        // args[0] - имя входного файла
        // args[1] - параметр, конфигурирующий вывод в файл или в консоль
        // args[2] - имя выходного файла (для вывода в файл)

        return args.length == 1 ||
                (args.length == 2 && args[1].equalsIgnoreCase(FLAG_CONSOLE)) ||
                (args.length == 3 && args[1].equalsIgnoreCase(FLAG_FILE));
    }

    private static String[] getOutputArgs(String[] args) {

        if (args.length == 1 || args[1].equalsIgnoreCase("-c")) {
            outputStreamName = "консоль";
            return new String[] {};
        }
        else if (args[1].equalsIgnoreCase("-f")) {
            outputStreamName = "файл " + args[2];
            return new String[] {args[2]};
        }

        throw new UnsupportedOperationException();
    }
}
