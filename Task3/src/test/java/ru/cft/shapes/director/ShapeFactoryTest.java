package ru.cft.shapes.director;

import org.junit.Test;
import ru.cft.shapes.IncorrectShapeNameException;
import ru.cft.shapes.product.Circle;
import ru.cft.shapes.product.Rectangle;
import ru.cft.shapes.product.Shape;
import ru.cft.shapes.product.Triangle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ShapeFactoryTest {

    @Test
    public void getShape_returnsShape_IfPassCorrectCircle() {

        double radius = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(radius);
        Circle expectedShape = new Circle(radius);

        Shape actualShape = new ShapeFactory().getShape("CIRCLE", parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test
    public void getShape_returnsShape_IfPassCorrectRectangle() {

        double length = 10;
        double width = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(length);
        parameters.add(width);
        Rectangle expectedShape = new Rectangle(length, width);

        Shape actualShape = new ShapeFactory().getShape("RECTANGLE", parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test
    public void getShape_returnsShape_IfPassCorrectTriangle() {

        double a = 3;
        double b = 4;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);
        Triangle expectedShape = new Triangle(a, b, c);

        Shape actualShape = new ShapeFactory().getShape("TRIANGLE", parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test(expected = IncorrectShapeNameException.class)
    public void getShape_throwsException_IfPassIncorrectShape() {
        new ShapeFactory().getShape("ELLIPSE", new ArrayList<>());
    }
}