package ru.cft.shapes.product;

import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    private static final double DELTA = 1e-20;

    @Test
    public void getLength_returnsLength() {

        double length = 5;
        double width = 10;

        Rectangle rectangle = new Rectangle(length, width);

        assertEquals(rectangle.getLength(), Math.max(length, width), DELTA);
    }

    @Test
    public void getWidth_returnsLength() {

        double length = 5;
        double width = 10;

        Rectangle rectangle = new Rectangle(length, width);

        assertEquals(rectangle.getWidth(), Math.min(length, width), DELTA);
    }

    @Test
    public void getDiagonal_returnsDiagonal() {

        double length = 5;
        double width = 10;
        double diagonal = Math.sqrt(length * length + width * width);

        Rectangle rectangle = new Rectangle(length, width);

        assertEquals(rectangle.getDiagonal(), diagonal, DELTA);
    }

    @Test
    public void getArea_returnsArea() {

        double length = 5;
        double width = 10;
        double area = length * width;

        Rectangle rectangle = new Rectangle(length, width);

        assertEquals(rectangle.getArea(), area, DELTA);
    }

    @Test
    public void getPerimeter_returnsPerimeter() {

        double length = 5;
        double width = 10;
        double perimeter = 2 * (length + width);

        Rectangle rectangle = new Rectangle(length, width);

        assertEquals(rectangle.getPerimeter(), perimeter, DELTA);
    }
}