package ru.cft.shapes.product;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    private static final double DELTA = 1e-10;

    @Test
    public void getSides_returnsSides() {

        double[] sides = new double[] {9, 5, 7};

        Triangle triangle = new Triangle(sides[0], sides[1], sides[2]);
        double[] calculatedSides = triangle.getSides();

        assertArrayEquals(calculatedSides, sides, DELTA);
    }

    @Test
    public void getAngles_returnsAngles() {

        double[] sides = new double[] {9, 5, 7};

        Triangle triangle = new Triangle(sides[0], sides[1], sides[2]);
        double[] calculatedSides = triangle.getSides();
        double[] calculatedAngles = triangle.getAngles();
        double[] sinDivideSide = new double[3];
        for (int i = 0; i < 3; i++) {
            sinDivideSide[i] = Math.sin(calculatedAngles[i] * Math.PI / 180) / calculatedSides[i];
        }

        assertEquals(sinDivideSide[0], sinDivideSide[1], DELTA);
        assertEquals(sinDivideSide[0], sinDivideSide[2], DELTA);
        assertEquals(sinDivideSide[1], sinDivideSide[2], DELTA);
    }

    @Test
    public void getArea_returnsArea() {

        double[] sides = new double[] {9, 5, 7};
        double perimeter = sides[0] + sides[1] + sides[2];
        double area = Math.sqrt(perimeter *
                (perimeter - 2 * sides[0]) *
                (perimeter - 2 * sides[1]) *
                (perimeter - 2 * sides[2])) / 4;

        Triangle triangle = new Triangle(sides[0], sides[1], sides[2]);

        assertEquals(triangle.getArea(), area, DELTA);
    }

    @Test
    public void getPerimeter_returnsPerimeter() {

        double[] sides = new double[] {9, 5, 7};
        double perimeter = sides[0] + sides[1] + sides[2];

        Triangle triangle = new Triangle(sides[0], sides[1], sides[2]);

        assertEquals(triangle.getPerimeter(), perimeter, DELTA);
    }
}