package ru.cft.shapes.product;

import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {

    private static final double DELTA = 1e-20;

    @Test
    public void getRadius_returnsRadius() {

        double radius = 5;

        Circle circle = new Circle(radius);

        assertEquals(circle.getRadius(), radius, DELTA);
    }

    @Test
    public void getDiameter_returnsDiameter() {

        double radius = 5;
        double diameter = 2 * radius;

        Circle circle = new Circle(radius);

        assertEquals(circle.getDiameter(), diameter, DELTA);
    }

    @Test
    public void getArea_returnsArea() {

        double radius = 5;
        double area = Math.PI * radius * radius;

        Circle circle = new Circle(radius);

        assertEquals(circle.getArea(), area, DELTA);
    }

    @Test
    public void getPerimeter_returnsPerimeter() {

        double radius = 5;
        double perimeter = 2 * Math.PI * radius;

        Circle circle = new Circle(radius);

        assertEquals(circle.getPerimeter(), perimeter, DELTA);
    }
}