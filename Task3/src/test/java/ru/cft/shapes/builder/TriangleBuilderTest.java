package ru.cft.shapes.builder;

import org.junit.Test;
import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Triangle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TriangleBuilderTest {

    @Test
    public void build_returnsTriangle_IfPassCorrectSides() {

        double a = 3;
        double b = 4;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);
        Triangle expectedShape = new Triangle(a, b, c);
        TriangleBuilder builder = new TriangleBuilder();

        Triangle actualShape = builder.build(parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeSideA() {

        double a = -3;
        double b = 4;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeSideB() {

        double a = 3;
        double b = -4;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeSideC() {

        double a = 3;
        double b = 4;
        double c = -5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassTooBigSideA() {

        double a = 30;
        double b = 4;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassTooBigSideB() {

        double a = 3;
        double b = 40;
        double c = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassTooBigSideC() {

        double a = 3;
        double b = 4;
        double c = 50;
        List<Double> parameters = new ArrayList<>();
        parameters.add(a);
        parameters.add(b);
        parameters.add(c);

        new TriangleBuilder().build(parameters);
    }
}