package ru.cft.shapes.builder;

import org.junit.Test;
import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Rectangle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RectangleBuilderTest {

    @Test
    public void build_returnsRectangle_IfPassCorrectLengthAndWidth() {

        double length = 5;
        double width = 10;
        List<Double> parameters = new ArrayList<>();
        parameters.add(length);
        parameters.add(width);
        Rectangle expectedShape = new Rectangle(length, width);
        RectangleBuilder builder = new RectangleBuilder();

        Rectangle actualShape = builder.build(parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeLength() {

        double length = -5;
        double width = 10;
        List<Double> parameters = new ArrayList<>();
        parameters.add(length);
        parameters.add(width);

        new RectangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeWidth() {

        double length = 5;
        double width = -10;
        List<Double> parameters = new ArrayList<>();
        parameters.add(length);
        parameters.add(width);

        new RectangleBuilder().build(parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeLengthAndWidth() {

        double length = -5;
        double width = -10;
        List<Double> parameters = new ArrayList<>();
        parameters.add(length);
        parameters.add(width);

        new RectangleBuilder().build(parameters);
    }

}