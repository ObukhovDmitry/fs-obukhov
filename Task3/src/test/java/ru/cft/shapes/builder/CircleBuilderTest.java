package ru.cft.shapes.builder;

import org.junit.Test;
import ru.cft.shapes.IncorrectShapeParametersException;
import ru.cft.shapes.product.Circle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CircleBuilderTest {

    @Test
    public void build_returnsCircle_IfPassCorrectRadius() {

        double radius = 5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(radius);
        Circle expectedShape = new Circle(radius);
        CircleBuilder builder = new CircleBuilder();

        Circle actualShape = builder.build(parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void build_throwsException_IfPassNegativeRadius() {

        double radius = -5;
        List<Double> parameters = new ArrayList<>();
        parameters.add(radius);

        new CircleBuilder().build(parameters);
    }
}