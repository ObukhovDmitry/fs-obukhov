package ru.cft.io;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;

import static ru.cft.io.FileManager.*;
import static org.junit.Assert.*;

public class InputStreamTest {

    private static final double DELTA = 1e-20;

    @Test
    public void getNextString_returnsString_IfPassCorrectString()
            throws InputException, IOException {

        String filename = "testReadCorrectString";
        createFile(filename);
        writeToFile(filename, filename);

        InputStream input = new InputStream(filename);
        String actualMessage = input.getNextString();
        deleteFile(filename);

        assertEquals(filename, actualMessage);
    }

    @Test(expected = NoSuchElementException.class)
    public void getNextString_throwsException_IfPassEmptyFile()
            throws InputException, IOException {

        String filename = "testReadIncorrectString";
        createFile(filename);

        try {
            InputStream input = new InputStream(filename);
            input.getNextString();
        } finally {
            deleteFile(filename);
        }
    }

    @Test
    public void getNextDouble_returnsDouble_IfPassCorrectDouble()
            throws InputException, IOException {

        String filename = "testReadCorrectDouble";
        double number = 42.0;
        createFile(filename);
        writeToFile(filename, Double.toString(number));

        InputStream input = new InputStream(filename);
        double actualMessage = input.getNextDouble();
        deleteFile(filename);

        assertEquals(number, actualMessage, DELTA);
    }

    @Test(expected = NoSuchElementException.class)
    public void getNextDouble_throwsException_IfPassString()
            throws InputException, IOException {

        String filename = "testReadIncorrectDouble";
        createFile(filename);
        writeToFile(filename, filename);

        try {
            InputStream input = new InputStream(filename);
            input.getNextDouble();
        } finally {
            deleteFile(filename);
        }
    }

    @Test
    public void hasNextDouble_returnsTrue_IfPassCorrectDouble()
            throws InputException, IOException {

        String filename = "testHasCorrectDouble";
        double number = 42.0;
        createFile(filename);
        writeToFile(filename, Double.toString(number));

        InputStream input = new InputStream(filename);
        boolean actualResult = input.hasNextDouble();
        deleteFile(filename);

        assertTrue(actualResult);
    }

    @Test
    public void hasNextDouble_returnsFalse_IfPassString()
            throws InputException, IOException {

        String filename = "testHasIncorrectDouble";
        createFile(filename);
        writeToFile(filename, filename);

        InputStream input = new InputStream(filename);
        boolean actualResult = input.hasNextDouble();
        deleteFile(filename);

        assertFalse(actualResult);
    }

    @Test(expected = InputException.class)
    public void InputStream_throwsException_IfFileNotExist()
            throws InputException, IOException {

        String filename = "testUndefinedFile";
        File file = new File(filename);
        if (file.exists()) file.delete();

        InputStream input = new InputStream(filename);
    }
}