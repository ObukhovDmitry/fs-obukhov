package ru.cft.io;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class FileManager {

    public static void createFile(String filename)
            throws IOException {

        File file = new File(filename);
        file.createNewFile();
    }

    public static void writeToFile(String filename, String message)
            throws IOException {

        PrintWriter writer = new PrintWriter(filename);
        writer.println(message);
        writer.close();
    }

    public static void deleteFile(String filename)
            throws IOException {

        File file = new File(filename);
        file.delete();
    }

    private FileManager() {}
}
