package ru.cft.io;

import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Scanner;

import static ru.cft.io.FileManager.*;
import static org.junit.Assert.*;

public class OutputStreamTest {

    @Test
    public void print_returnsString_IfPassString()
            throws IOException {

        String filename = "testPrintString";
        createFile(filename);

        OutputStream output = new OutputStream(new String[] {filename});
        output.print(filename);
        output.close();

        Scanner in = new Scanner(Paths.get(filename));
        String actualMessage = in.nextLine();
        deleteFile(filename);

        assertEquals(filename, actualMessage);
    }
}