package ru.cft.shapeio;

import org.junit.Test;
import ru.cft.io.InputStream;
import ru.cft.shapes.*;
import ru.cft.shapes.product.Circle;
import ru.cft.shapes.product.Rectangle;
import ru.cft.shapes.product.Shape;
import ru.cft.shapes.product.Triangle;

import static org.junit.Assert.*;

public class ShapeReaderTest {

    @Test
    public void read_returnsShape_IfPassCorrectCircle()
            throws ShapeReaderException {

        String shapeName = "CIRCLE";
        double[] parameters = {5};

        Shape expectedShape = new Circle(parameters[0]);
        Shape actualShape = getCircle(shapeName, parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test
    public void read_returnsShape_IfPassCorrectRectangle()
            throws ShapeReaderException {

        String shapeName = "RECTANGLE";
        double[] parameters = {5, 10};

        Shape expectedShape = new Rectangle(parameters[0], parameters[1]);
        Shape actualShape = getRectangle(shapeName, parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test
    public void read_returnsShape_IfPassCorrectTriangle()
            throws ShapeReaderException {

        String shapeName = "TRIANGLE";
        double[] parameters = {5, 4, 3};

        Shape expectedShape = new Triangle(parameters[0], parameters[1], parameters[2]);
        Shape actualShape = getTriangle(shapeName, parameters);

        assertEquals(expectedShape, actualShape);
    }

    @Test(expected = IncorrectShapeNameException.class)
    public void read_throwsException_IfPassIncorrectShapeWithOneParameter()
            throws ShapeReaderException {

        String shapeName = "ELLIPSE";
        double[] parameters = {5};

        getCircle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeNameException.class)
    public void read_throwsException_IfPassIncorrectShapeWithTwoParameters()
            throws ShapeReaderException {

        String shapeName = "ELLIPSE";
        double[] parameters = {5, 4};

        getRectangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeNameException.class)
    public void read_throwsException_IfPassIncorrectShapeWithThreeParameters()
            throws ShapeReaderException {

        String shapeName = "ELLIPSE";
        double[] parameters = {5, 4, 3};

        getTriangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassCircleWithNegativeRadius()
            throws ShapeReaderException {

        String shapeName = "CIRCLE";
        double[] parameters = {-5};

        getCircle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassCircleWithoutRadius()
            throws ShapeReaderException {

        String shapeName = "CIRCLE";
        double[] parameters = {};

        getCircle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassCircleWithTwoParameters()
            throws ShapeReaderException {

        String shapeName = "CIRCLE";
        double[] parameters = {5, 5};

        getCircle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassRectangleWithNegativeSide()
            throws ShapeReaderException {

        String shapeName = "RECTANGLE";
        double[] parameters = {5, -4};

        getRectangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassRectangleWithOneParameter()
            throws ShapeReaderException {

        String shapeName = "RECTANGLE";
        double[] parameters = {5};

        getRectangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassRectangleWithThreeParameter()
            throws ShapeReaderException {

        String shapeName = "RECTANGLE";
        double[] parameters = {5, 4, 2};

        getRectangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassTriangleWithNegativeSide()
            throws ShapeReaderException {

        String shapeName = "TRIANGLE";
        double[] parameters = {5, 4, -3};

        getTriangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassTriangleWithoutTooBigSide()
            throws ShapeReaderException {

        String shapeName = "TRIANGLE";
        double[] parameters = {5, 4, 10};

        getTriangle(shapeName, parameters);
    }

    @Test(expected = IncorrectShapeParametersException.class)
    public void read_throwsException_IfPassTriangleWithoutParameters()
            throws ShapeReaderException {

        String shapeName = "TRIANGLE";
        double[] parameters = {};

        getTriangle(shapeName, parameters);
    }

    private Circle getCircle(String shapeName, double[] parameters)
            throws ShapeReaderException {

        InputStreamStub input = new InputStreamStub(shapeName, parameters);
        ShapeReader reader = new ShapeReader(input);

        return (Circle) reader.read();
    }

    private Rectangle getRectangle(String shapeName, double[] parameters)
            throws ShapeReaderException {

        InputStreamStub input = new InputStreamStub(shapeName, parameters);
        ShapeReader reader = new ShapeReader(input);

        return (Rectangle) reader.read();
    }

    private Triangle getTriangle(String shapeName, double[] parameters)
            throws ShapeReaderException {

        InputStreamStub input = new InputStreamStub(shapeName, parameters);
        ShapeReader reader = new ShapeReader(input);

        return (Triangle) reader.read();
    }

    private class InputStreamStub extends InputStream {

        private String shapeName;
        private double[] shapeParameters;
        private int it = 0;

        public InputStreamStub(String shapeName, double[] shapeParameters) {

            this.shapeParameters = shapeParameters;
            this.shapeName = shapeName;
        }

        @Override
        public String getNextString() {
            return shapeName;
        }

        @Override
        public double getNextDouble() {
            return shapeParameters[it++];
        }

        @Override
        public boolean hasNextDouble() {
            return it < shapeParameters.length;
        }

        @Override
        public void close() {
        }
    }
}