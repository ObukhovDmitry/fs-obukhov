package ru.cft.shapeio;

import ru.cft.io.OutputStream;
import ru.cft.shapes.product.Circle;
import ru.cft.shapes.product.Rectangle;
import ru.cft.shapes.product.Shape;
import ru.cft.shapes.product.Triangle;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ShapeWriterTest {

    @Test
    public void print_verifyPrint_IfPassCircle() {

        Shape shape = new Circle(5);
        OutputStream mockedStream = mock(OutputStream.class);
        ShapeWriter writer = new ShapeWriter(mockedStream);

        writer.print(shape);

        verify(mockedStream).print(shape.toString());
    }

    @Test
    public void print_verifyPrint_IfPassRectangle() {

        Shape shape = new Rectangle(5, 4);
        OutputStream mockedStream = mock(OutputStream.class);
        ShapeWriter writer = new ShapeWriter(mockedStream);

        writer.print(shape);

        verify(mockedStream).print(shape.toString());
    }

    @Test
    public void print_verifyPrint_IfPassTriangle() {

        Shape shape = new Triangle(5, 4, 3);
        OutputStream mockedStream = mock(OutputStream.class);
        ShapeWriter writer = new ShapeWriter(mockedStream);

        writer.print(shape);

        verify(mockedStream).print(shape.toString());
    }
}