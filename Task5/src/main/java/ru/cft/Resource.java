package ru.cft;

class Resource {

    private static final Object SYNC = new Object();
    private static int nextId = 0;
    private int id;

    Resource() {
        synchronized (SYNC) {
            id = nextId++;
        }
    }

    @Override
    public String toString() {
        return "Resource{" +
                "id=" + id +
                '}';
    }
}
