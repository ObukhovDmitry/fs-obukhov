package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

class ParametersDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParametersDao.class);
    private static final String PROPERTY_FILE = "task5.properties";
    private static final String STORAGE_CAPACITY_KEY = "S";
    private static final String PRODUCERS_COUNT_KEY = "N";
    private static final String CONSUMERS_COUNT_KEY = "M";
    private static final String PRODUCE_DELAY = "tN";
    private static final String CONSUME_DELAY = "tM";

    Parameters getParameters() {

        Parameters parameters = Parameters.DEFAULT;
        Properties properties = new Properties();

        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream(PROPERTY_FILE)) {

            properties.load(inputStream);

            int storageCapacity = Integer.parseInt(properties.getProperty(STORAGE_CAPACITY_KEY));
            int producersCount = Integer.parseInt(properties.getProperty(PRODUCERS_COUNT_KEY));
            int consumersCount = Integer.parseInt(properties.getProperty(CONSUMERS_COUNT_KEY));
            long produceDelay = Integer.parseInt(properties.getProperty(PRODUCE_DELAY));
            long consumeDelay = Integer.parseInt(properties.getProperty(CONSUME_DELAY));

            parameters = new Parameters(storageCapacity,
                    producersCount,  consumersCount, produceDelay, consumeDelay);

        } catch (Exception e) {
            LOGGER.error("Oops! Property can't be loaded.");
        }

        return parameters;
    }
}
