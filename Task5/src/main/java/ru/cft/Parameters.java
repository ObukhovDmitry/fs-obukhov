package ru.cft;

class Parameters {

    static final Parameters DEFAULT = new Parameters(10, 1, 1, 1000, 1000);
    private int storageCapacity;
    private int producersCount;
    private int consumersCount;
    private long produceDelay;
    private long consumeDelay;

    Parameters(
            int storageCapacity,
            int producersCount,
            int consumersCount,
            long produceDelay,
            long consumeDelay) {

        this.storageCapacity = storageCapacity;
        this.producersCount = producersCount;
        this.consumersCount = consumersCount;
        this.produceDelay = produceDelay;
        this.consumeDelay = consumeDelay;
    }

    int getStorageCapacity() {
        return storageCapacity;
    }

    int getProducersCount() {
        return producersCount;
    }

    int getConsumersCount() {
        return consumersCount;
    }

    long getProduceDelay() {
        return produceDelay;
    }

    long getConsumeDelay() {
        return consumeDelay;
    }
}
