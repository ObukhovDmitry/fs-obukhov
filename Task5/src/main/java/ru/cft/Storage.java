package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

class Storage {

    private static final Logger LOGGER = LoggerFactory.getLogger(Storage.class);
    private int maxCapacity;
    private Queue<Resource> resources;

    Storage(int maxCapacity) {

        this.maxCapacity = maxCapacity;
        resources = new LinkedList<>();
    }

    synchronized void put(Resource resource) {

        try {
            while (resources.size() == maxCapacity) {
                wait();
            }
        } catch (InterruptedException exception) {
            LOGGER.error("Thread was interrupted.");
            Thread.currentThread().interrupt();
        }
        resources.add(resource);
        notifyAll();
    }

    synchronized Resource get() {

        try {
            while (resources.isEmpty()) {
                wait();
            }
        } catch (InterruptedException exception) {
            LOGGER.error("Thread was interrupted.");
            Thread.currentThread().interrupt();
        }
        Resource resource = resources.poll();
        notifyAll();
        return resource;
    }
}
