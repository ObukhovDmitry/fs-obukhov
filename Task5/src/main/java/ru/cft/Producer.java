package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Producer implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    private int id;
    private Storage storage;
    private long delay;

    Producer(int id, Storage storage, long delay) {

        this.id = id;
        this.storage = storage;
        this.delay = delay;
    }

    @Override
    public void run() {

        String producerIdString = "[Producer " + id + "]";

        try {
            while (true) {
                LOGGER.info("{} is waiting.", producerIdString);
                Thread.sleep(delay);
                LOGGER.info("{} resumed.", producerIdString);
                Resource resource = new Resource();
                storage.put(resource);
                LOGGER.info("{} produced: {}", producerIdString, resource);
            }
        } catch (InterruptedException exception) {
            LOGGER.error("{} interrupted.", producerIdString);
            Thread.currentThread().interrupt();
        }
    }
}
