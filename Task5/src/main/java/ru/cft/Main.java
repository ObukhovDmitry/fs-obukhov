package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        ParametersDao parametersDao = new ParametersDao();
        Parameters parameters = parametersDao.getParameters();
        Storage storage = new Storage(parameters.getStorageCapacity());

        for (int i = 0; i < parameters.getProducersCount(); i++) {
            Thread producerThread = new Thread(
                    new Producer(i, storage, parameters.getProduceDelay()));
            producerThread.start();
            LOGGER.debug("[Producer {}] started.", i);
        }

        for (int i = 0; i < parameters.getConsumersCount(); i++) {
            Thread consumerThread = new Thread(
                    new Consumer(i, storage, parameters.getConsumeDelay()));
            consumerThread.start();
            LOGGER.debug("[Consumer {}] started.", i);
        }
    }
}
