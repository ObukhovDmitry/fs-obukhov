package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Consumer implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Consumer.class);
    private int id;
    private Storage storage;
    private long delay;

    Consumer(int id, Storage storage, long delay) {

        this.id = id;
        this.storage = storage;
        this.delay = delay;
    }

    @Override
    public void run() {

        String consumerIdString = "[Consumer " + id + "]";

        try {
            while (true) {
                LOGGER.info("{} is waiting.", consumerIdString);
                Thread.sleep(delay);
                LOGGER.info("{} resumed.", consumerIdString);
                Resource resource = storage.get();
                LOGGER.info("{} consumed: {}", consumerIdString, resource);
            }
        } catch (InterruptedException exception) {
            LOGGER.error("{} interrupted.", consumerIdString);
            Thread.currentThread().interrupt();
        }
    }
}
