package ru.cft.model;

import org.junit.Test;
import org.mockito.verification.VerificationMode;
import ru.cft.view.IView;
import ru.cft.view.ViewCell;
import ru.cft.view.ViewCellType;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class SapperTest {

    @Test
    public void start_verifyViewRestart() throws Exception {

        Options options = Options.EASY;
        IView mockedView = mock(IView.class);

        Sapper sapper = new Sapper();
        sapper.setView(mockedView);
        sapper.setOptions(options);
        sapper.start();

        verify(mockedView).restart(options.getRows(), options.getCols(), options.getNumberOfBombs());
    }

    @Test
    public void doMove_verifyDoMove_ifPassOpenMove() throws Exception {

        Cell startCell = new Cell(0, 0);
        ViewCell viewStartCell = new ViewCell(startCell.getRow(), startCell.getCol(), ViewCellType.NEAR_BOMB_1);
        List<ViewCell> toUpdate = Arrays.asList(viewStartCell);

        doMoveAndVerifyWith(toUpdate, MoveType.OPEN, startCell);
    }

    @Test
    public void doMove_verifyDoMove_ifPassFlagMove() throws Exception {

        Cell startCell = new Cell(0, 0);
        ViewCell viewStartCell = new ViewCell(startCell.getRow(), startCell.getCol(), ViewCellType.FLAG);
        List<ViewCell> toUpdate = Arrays.asList(viewStartCell);

        doMoveAndVerifyWith(toUpdate, MoveType.FLAG, startCell);
    }

    @Test
    public void doMove_verifyDoMove_ifPassOpenAroundMove() throws Exception {

        Cell startCell = new Cell(0, 2);
        List<ViewCell> toUpdate = Arrays.asList(
                new ViewCell(0, 1, ViewCellType.NEAR_BOMB_1),
                new ViewCell(1, 2, ViewCellType.NEAR_BOMB_1));

        doMoveAndVerifyWith(toUpdate, MoveType.OPEN_AROUND, startCell);
    }

    private void doMoveAndVerifyWith(List<ViewCell> toUpdate, MoveType moveType, Cell startCell) {

        Options options = new Options(3, 3, 1);
        Field field = new Field(options.getRows(), options.getCols());
        field.addBomb(1, 1);
        field.setFlag(new Cell(1, 1));
        field.setOpen(new Cell(0, 2));

        IView mockedView = mock(IView.class);
        doNothing().when(mockedView).updateCells(toUpdate);

        Sapper sapper = new Sapper();
        sapper.setView(mockedView);
        sapper.setOptions(options);
        sapper.start();
        sapper.setField(field);
        sapper.doMove(moveType, startCell);

        verify(mockedView).updateCells(toUpdate);
    }

    @Test
    public void generateField_verifyGenerate() throws Exception {

        Options options = Options.EASY;
        Cell startCell = new Cell(0, 0);

        Field field = new Field(options.getRows(), options.getCols());
        Generator mockedGenerator = mock(Generator.class);
        when(mockedGenerator.generate(startCell, options)).thenReturn(field);

        IView mockedView = mock(IView.class);
        doNothing().when(mockedView).restart(options.getRows(), options.getCols(), options.getNumberOfBombs());

        Sapper sapper = new Sapper();
        sapper.setView(mockedView);
        sapper.setOptions(options);
        sapper.setGenerator(mockedGenerator);
        sapper.start();
        sapper.generateField(startCell);

        verify(mockedGenerator).generate(startCell, options);
    }
}