package ru.cft.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class FieldTest {

    @Test
    public void addBomb_placeBomb_ifPassCorrectBomb() throws Exception {

        Cell bomb = new Cell(0, 0);

        Field field = getFieldWithBombs(Arrays.asList(bomb));

        assertTrue(field.isBomb(bomb));
    }

    @Test
    public void addBomb_placeBombNextDoor_ifPassCornerBomb() throws Exception {

        Cell bomb = new Cell(0, 0);
        int actualNumberOfBombsNextDoor = 1;

        Field field = getFieldWithBombs(Arrays.asList(bomb));

        List<Cell> neighbors = field.getNeighbors(bomb);
        for (Cell neighbor : neighbors) {
            assertEquals(field.getNumberOfBombsAround(neighbor), actualNumberOfBombsNextDoor);
        }
    }

    @Test
    public void addBomb_placeBombNextDoor_ifPassSideBomb() throws Exception {

        Cell bomb = new Cell(0, 1);
        int actualNumberOfBombsNextDoor = 1;

        Field field = getFieldWithBombs(Arrays.asList(bomb));

        List<Cell> neighbors = field.getNeighbors(bomb);
        for (Cell neighbor : neighbors) {
            assertEquals(field.getNumberOfBombsAround(neighbor), actualNumberOfBombsNextDoor);
        }
    }

    @Test
    public void addBomb_placeBombNextDoor_ifPassCenterBomb() throws Exception {

        Cell bomb = new Cell(1, 1);
        int actualNumberOfBombsNextDoor = 1;

        Field field = getFieldWithBombs(Arrays.asList(bomb));

        List<Cell> neighbors = field.getNeighbors(bomb);
        for (Cell neighbor : neighbors) {
            assertEquals(field.getNumberOfBombsAround(neighbor), actualNumberOfBombsNextDoor);
        }
    }

    @Test
    public void getBombs_returnCells_ifPassOneBomb() throws Exception {

        List<Cell> exceptedBombs = Arrays.asList(
                new Cell(1, 1));

        Field field = getFieldWithBombs(exceptedBombs);
        List<Cell> actualBombs = field.getBombs();

        assertArrayEquals(exceptedBombs.toArray(), actualBombs.toArray());
    }

    @Test
    public void getBombs_returnCells_ifPassTwoBomb() throws Exception {

        List<Cell> exceptedBombs = Arrays.asList(
                new Cell(1, 1),
                new Cell(1, 2));

        Field field = getFieldWithBombs(exceptedBombs);
        List<Cell> actualBombs = field.getBombs();

        assertArrayEquals(exceptedBombs.toArray(), actualBombs.toArray());
    }

    @Test
    public void getNeighbors_returnCells_ifPassCornerCell() throws Exception {

        Cell centerCell = new Cell(0, 0);
        List<Cell> exceptedCells = Arrays.asList(
                new Cell(0, 1),
                new Cell(1, 0),
                new Cell(1, 1));

        Field field = getFieldWithBombs(null);
        List<Cell> neighbors = field.getNeighbors(centerCell);

        assertArrayEquals(exceptedCells.toArray(), neighbors.toArray());
    }

    @Test
    public void getNeighbors_returnCells_ifPassSideCell() throws Exception {

        Cell centerCell = new Cell(1, 0);
        List<Cell> exceptedCells = Arrays.asList(
                new Cell(0, 0),
                new Cell(0, 1),
                new Cell(1, 1),
                new Cell(2, 0),
                new Cell(2, 1));

        Field field = getFieldWithBombs(null);
        List<Cell> neighbors = field.getNeighbors(centerCell);

        assertArrayEquals(exceptedCells.toArray(), neighbors.toArray());
    }

    @Test
    public void getNeighbors_returnCells_ifPassCenterCell() throws Exception {

        Cell centerCell = new Cell(1, 1);
        List<Cell> exceptedCells = Arrays.asList(
                new Cell(0, 0),
                new Cell(0, 1),
                new Cell(0, 2),
                new Cell(1, 0),
                new Cell(1, 2),
                new Cell(2, 0),
                new Cell(2, 1),
                new Cell(2, 2));

        Field field = getFieldWithBombs(null);
        List<Cell> neighbors = field.getNeighbors(centerCell);

        assertArrayEquals(exceptedCells.toArray(), neighbors.toArray());
    }

    private Field getFieldWithBombs(List<Cell> bombs) {

        int rows = 3;
        int cols = 3;
        Field field = new Field(rows, cols);

        if (bombs == null) {
            return field;
        }

        for (Cell bomb : bombs) {
            field.addBomb(bomb.getRow(), bomb.getCol());
        }

        return field;
    }
}