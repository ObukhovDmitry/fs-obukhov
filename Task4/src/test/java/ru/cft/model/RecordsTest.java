package ru.cft.model;

import org.junit.Test;
import ru.cft.dao.IRecordDao;
import ru.cft.view.IView;
import ru.cft.view.ViewRecord;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RecordsTest {

    @Test
    public void showRecords_verifyViewShowRecords() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> time = Arrays.asList(10, 50, 300);

        Map<Options, Record> recordByMode = new Hashtable<>();
        recordByMode.put(Options.EASY, new Record(userNames.get(0),time.get(0)));
        recordByMode.put(Options.NORM, new Record(userNames.get(1),time.get(1)));
        recordByMode.put(Options.HARD, new Record(userNames.get(2),time.get(2)));

        List<ViewRecord> viewRecords = new ArrayList<>();
        viewRecords.add(new ViewRecord(userNames.get(0),time.get(0)));
        viewRecords.add(new ViewRecord(userNames.get(1),time.get(1)));
        viewRecords.add(new ViewRecord(userNames.get(2),time.get(2)));

        IRecordDao mockedRecordsDao = mock(IRecordDao.class);
        when(mockedRecordsDao.readRecords()).thenReturn(recordByMode);
        IView mockedView = mock(IView.class);

        Records records = new Records(mockedRecordsDao);
        records.setView(mockedView);
        records.showRecords();

        verify(mockedView).showRecords(viewRecords);
    }

    @Test
    public void updateRecord_verifyWriteRecords_ifPassEasyOptions() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> time = Arrays.asList(10, 50, 300);
        Options options = Options.EASY;
        Record oldRecord = new Record(userNames.get(0),time.get(0));
        Record newRecord = new Record("new", 5);

        Map<Options, Record> recordByMode = new Hashtable<>();
        recordByMode.put(Options.EASY, oldRecord);
        recordByMode.put(Options.NORM, new Record(userNames.get(1),time.get(1)));
        recordByMode.put(Options.HARD, new Record(userNames.get(2),time.get(2)));

        IRecordDao mockedRecordsDao = mock(IRecordDao.class);
        when(mockedRecordsDao.readRecords()).thenReturn(recordByMode);

        Records records = new Records(mockedRecordsDao);
        records.setOptions(options);
        records.updateRecord(newRecord);
        recordByMode.replace(options, newRecord);

        verify(mockedRecordsDao).writeRecords(recordByMode);
    }

    @Test
    public void updateRecord_verifyWriteRecords_ifPassNormOptions() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> time = Arrays.asList(10, 50, 300);
        Options options = Options.NORM;
        Record oldRecord = new Record(userNames.get(1),time.get(1));
        Record newRecord = new Record("new", 5);

        Map<Options, Record> recordByMode = new Hashtable<>();
        recordByMode.put(Options.EASY, new Record(userNames.get(0),time.get(0)));
        recordByMode.put(Options.NORM, oldRecord);
        recordByMode.put(Options.HARD, new Record(userNames.get(2),time.get(2)));

        IRecordDao mockedRecordsDao = mock(IRecordDao.class);
        when(mockedRecordsDao.readRecords()).thenReturn(recordByMode);

        Records records = new Records(mockedRecordsDao);
        records.setOptions(options);
        records.updateRecord(newRecord);
        recordByMode.replace(options, newRecord);

        verify(mockedRecordsDao).writeRecords(recordByMode);
    }

    @Test
    public void updateRecord_verifyWriteRecords_ifPassHardOptions() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> time = Arrays.asList(10, 50, 300);
        Options options = Options.HARD;
        Record oldRecord = new Record(userNames.get(1),time.get(1));
        Record newRecord = new Record("new", 5);

        Map<Options, Record> recordByMode = new Hashtable<>();
        recordByMode.put(Options.EASY, new Record(userNames.get(0),time.get(0)));
        recordByMode.put(Options.NORM, new Record(userNames.get(1),time.get(1)));
        recordByMode.put(Options.HARD, oldRecord);

        IRecordDao mockedRecordsDao = mock(IRecordDao.class);
        when(mockedRecordsDao.readRecords()).thenReturn(recordByMode);

        Records records = new Records(mockedRecordsDao);
        records.setOptions(options);
        records.updateRecord(newRecord);
        recordByMode.replace(options, newRecord);

        verify(mockedRecordsDao).writeRecords(recordByMode);
    }

    @Test
    public void isRecord_returnTrue_ifPassRecord() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> times = Arrays.asList(10, 50, 300);
        Options options = Options.HARD;
        int newRecordTime = 5;

        assertTrue(isRecordTime(userNames, times, options, newRecordTime));
    }

    @Test
    public void isRecord_returnFalse_ifPassNotRecord() throws Exception {

        List<String> userNames = Arrays.asList("easy", "norm", "hard");
        List<Integer> times = Arrays.asList(10, 50, 300);
        Options options = Options.HARD;
        int newRecordTime = 500;

        assertFalse(isRecordTime(userNames, times, options, newRecordTime));
    }

    private boolean isRecordTime(List<String> userNames, List<Integer> times, Options options, int newRecordTime) {

        Map<Options, Record> recordByMode = new Hashtable<>();
        recordByMode.put(Options.EASY, new Record(userNames.get(0),times.get(0)));
        recordByMode.put(Options.NORM, new Record(userNames.get(1),times.get(1)));
        recordByMode.put(Options.HARD, new Record(userNames.get(2),times.get(2)));

        IRecordDao mockedRecordsDao = mock(IRecordDao.class);
        when(mockedRecordsDao.readRecords()).thenReturn(recordByMode);

        Records records = new Records(mockedRecordsDao);
        records.setOptions(options);

        return records.isRecord(newRecordTime);
    }
}
