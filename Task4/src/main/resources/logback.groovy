import ch.qos.logback.classic.filter.ThresholdFilter

appender("CONSOLE", ConsoleAppender) {
    target = "System.out"
    filter(ThresholdFilter) {
        level = INFO
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%-5level %logger - %msg%n"
    }
}

appender("FILE", FileAppender) {
    file = "task4_log.txt"
    append = false
    filter(ThresholdFilter) {
        level = DEBUG
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%-5level %logger - %msg%n"
    }
}

root(DEBUG, ["CONSOLE", "FILE"])
