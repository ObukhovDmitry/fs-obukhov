package ru.cft.dao;

import ru.cft.model.Options;
import ru.cft.model.Record;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RecordDao implements IRecordDao {

    private static final String FILE_RECORDS = "records.txt";
    private static final String CHARSET = "UTF-8";
    private static final Record defaultRecord = new Record("Аноним", 999);

    public Map<Options, Record> readRecords() {

        Map<Options, Record> recordByMode = new HashMap<>();

        try(Scanner in = new Scanner(Paths.get(FILE_RECORDS), CHARSET)) {

            int timeEasy = in.nextInt();
            String nameEasy = in.next();
            int timeNorm = in.nextInt();
            String nameNorm = in.next();
            int timeHard = in.nextInt();
            String nameHard = in.next();

            if (!checkRecords(timeEasy, timeNorm, timeHard)) {
                throw new IllegalArgumentException();
            }

            recordByMode.put(Options.EASY, new Record(nameEasy, timeEasy));
            recordByMode.put(Options.NORM, new Record(nameNorm, timeNorm));
            recordByMode.put(Options.HARD, new Record(nameHard, timeHard));

        } catch (Exception e) {
            return defaultRecords();
        }

        return recordByMode;
    }

    public void writeRecords(Map<Options, Record> recordByMode) {

        try (PrintWriter writer = new PrintWriter(FILE_RECORDS)) {
            writer.println(recordByMode.get(Options.EASY).toString());
            writer.println(recordByMode.get(Options.NORM).toString());
            writer.println(recordByMode.get(Options.HARD).toString());
        } catch (FileNotFoundException ignore) {}
    }

    private Map<Options, Record> defaultRecords() {

        Map<Options, Record> recordByMode = new HashMap<>();

        recordByMode.put(Options.EASY, defaultRecord);
        recordByMode.put(Options.NORM, defaultRecord);
        recordByMode.put(Options.HARD, defaultRecord);

        return recordByMode;
    }

    private boolean checkRecords(int beginnerRecord, int amateurRecord, int professionalRecord) {
        return beginnerRecord > 0 && beginnerRecord <= defaultRecord.getTime() &&
                amateurRecord > 0 && amateurRecord <= defaultRecord.getTime() &&
                professionalRecord > 0 && professionalRecord <= defaultRecord.getTime();
    }
}
