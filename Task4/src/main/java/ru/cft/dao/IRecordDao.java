package ru.cft.dao;

import ru.cft.model.Options;
import ru.cft.model.Record;

import java.util.Map;

/**
 * Интерфейс IRecordDao предназначен для получения и записи рекордов
 */
public interface IRecordDao {

    /**
     * В этом методе происходит считывание рекордов
     *
     * @return возвращает мапу с рекордами, для каждого типа игры
     */
    Map<Options, Record> readRecords();

    /**
     * В этом методе происходит запись рекордов
     *
     * @param recordByMode мапа с рекордами, для каждого типа игры
     */
    void writeRecords(Map<Options, Record> recordByMode);
}
