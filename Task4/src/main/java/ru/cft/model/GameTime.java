package ru.cft.model;

import ru.cft.view.IView;

public class GameTime implements IModel {

    private IView view;
    private long time;

    @Override
    public void setView(IView view) {
        this.view = view;
    }

    public long getStartTime() {
        return time;
    }

    public void setStartTime(long time) {
        this.time = time;
    }

    public void updateTime(int currentTimeInSeconds) {
        view.updateTimer(currentTimeInSeconds);
    }
}
