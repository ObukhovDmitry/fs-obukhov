package ru.cft.model;

import java.util.ArrayList;
import java.util.List;

class Field {

    private int rows;
    private int cols;
    private CellState[][] cells;
    private int numberOfFlaggedCells;
    private int numberOfOpenedCells;

    Field(int rows, int cols) {

        this.rows = rows;
        this.cols = cols;
        cells = new CellState[rows][cols];
        numberOfFlaggedCells = 0;
        numberOfOpenedCells = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                cells[i][j] = new CellState();
            }
        }
    }

    int getNumberOfFlaggedCells() {
        return numberOfFlaggedCells;
    }

    int getNumberOfOpenedCells() {
        return numberOfOpenedCells;
    }

    void addBomb(int row, int col) {

        placeBomb(row, col);

        for (Cell neighbor : getNeighbors(row, col)) {
            placeBombNextDoor(neighbor);
        }
    }

    List<Cell> getBombs() {

        List<Cell> bombs = new ArrayList<>();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (isBomb(i, j)) {
                    bombs.add(new Cell(i, j));
                }
            }
        }

        return bombs;
    }

    List<Cell> getNeighbors(Cell cell) {
        return getNeighbors(cell.getRow(), cell.getCol());
    }


    CellView getCellType(Cell cell) {
        return getCell(cell).cellView;
    }

    boolean isOpened(Cell cell) {
        return getCell(cell).cellView == CellView.OPENED;
    }

    boolean isFlagged(Cell cell) {
        return getCell(cell).cellView == CellView.FLAGGED;
    }

    boolean isLocked(Cell cell) {
        return getCell(cell).cellView == CellView.LOCKED;
    }

    void setOpen(Cell cell) {

        numberOfOpenedCells++;
        getCell(cell).cellView = CellView.OPENED;
    }

    void setFlag(Cell cell) {

        numberOfFlaggedCells++;
        getCell(cell).cellView = CellView.FLAGGED;
    }

    void setLock(Cell cell) {

        numberOfFlaggedCells--;
        getCell(cell).cellView = CellView.LOCKED;
    }


    int getNumberOfBombsAround(Cell cell) {
        return getCell(cell).numberOfBombsAround;
    }

    boolean isBomb(Cell cell) {
        return isBomb(cell.getRow(), cell.getCol());
    }


    private CellState getCell(Cell cell) {
        return cells[cell.getRow()][cell.getCol()];
    }

    private List<Cell> getNeighbors(int row, int col) {

        ArrayList<Cell> neighbors = new ArrayList<>();

        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i == row && j == col) {
                    continue;
                }
                if (isCorrectCell(i, j)) {
                    neighbors.add(new Cell(i, j));
                }
            }
        }

        return neighbors;
    }

    private boolean isBomb(int row, int col) {
        return cells[row][col].numberOfBombsAround == -1;
    }

    private boolean isCorrectCell(int row, int col) {

        return row >= 0 && row < rows &&
                col >= 0 && col < cols;
    }

    private void placeBomb(int row, int col) {
        cells[row][col].numberOfBombsAround = -1;
    }

    private void placeBombNextDoor(Cell cell) {
        if (!isBomb(cell)) {
            getCell(cell).numberOfBombsAround++;
        }
    }

    private static class CellState {

        CellView cellView = CellView.LOCKED;
        int numberOfBombsAround = 0;
    }
}
