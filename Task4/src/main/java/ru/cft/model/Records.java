package ru.cft.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.dao.IRecordDao;
import ru.cft.view.IView;
import ru.cft.view.ViewRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Records implements IModel, IRecords, IOptionsObserver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Records.class);
    private IView view;
    private IRecordDao recordDao;
    private Options options;
    private Map<Options, Record> recordByMode;

    public Records(IRecordDao recordDao) {

        this.recordDao = recordDao;
        recordByMode = recordDao.readRecords();
    }

    @Override
    public void setView(IView view) {
        this.view = view;
    }

    @Override
    public void setOptions(Options newOptions) {
        options = newOptions;
    }

    @Override
    public void updateRecord(Record newRecord) {

        LOGGER.info("Новый рекорд: " + newRecord.toString());

        recordByMode.replace(options, newRecord);
        recordDao.writeRecords(recordByMode);
    }

    @Override
    public void showRecords() {

        Record easy = recordByMode.get(Options.EASY);
        Record norm = recordByMode.get(Options.NORM);
        Record hard = recordByMode.get(Options.HARD);

        List<ViewRecord> viewRecords = new ArrayList<>();
        viewRecords.add(new ViewRecord(easy.getUserName(), easy.getTime()));
        viewRecords.add(new ViewRecord(norm.getUserName(), norm.getTime()));
        viewRecords.add(new ViewRecord(hard.getUserName(), hard.getTime()));

        view.showRecords(viewRecords);
    }

    @Override
    public boolean isRecord(int time) {
        return recordByMode.containsKey(options) && time <= recordByMode.get(options).getTime();
    }
}
