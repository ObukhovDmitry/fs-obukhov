package ru.cft.model;

/**
 * Интерфейс IGame предоставляет набор методов для возможных действий в игре
 */
public interface IGame {

    /**
     * Метод для запуска игры
     */
    void start();

    /**
     * В этом методе происходит обработка ходов игрока
     * @param moveType тип хода
     * @param cell ячейка, на которую нажал игрок
     */
    void doMove(MoveType moveType, Cell cell);

    /**
     * В этом методе происходит генерация игрового поля
     * @param startCell клектка, в которой был сделан первый ход (в ней гарантированно не будет мины)
     */
    void generateField(Cell startCell);
}
