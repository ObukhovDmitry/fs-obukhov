package ru.cft.model;

import java.util.ArrayList;
import java.util.List;

public class OptionsObservable implements IOptionsObservable {

    private List<IOptionsObserver> observers;

    public OptionsObservable() {
        observers = new ArrayList<>();
    }

    @Override
    public void addObserver(IOptionsObserver observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(Options newOptions) {
        for (IOptionsObserver observer : observers) {
            observer.setOptions(newOptions);
        }
    }
}
