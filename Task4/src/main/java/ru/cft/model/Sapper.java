package ru.cft.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.view.IView;
import ru.cft.view.ViewCellType;
import ru.cft.view.ViewCell;

import java.util.*;

public class Sapper implements IModel, IGame, IOptionsObserver {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sapper.class);
    private IView view;
    private Options options;
    private Field field;
    private boolean isLose;
    private List<ViewCell> cellsToUpdate;
    private Generator generator = new Generator();

    @Override
    public void setView(IView view) {
        this.view = view;
    }

    @Override
    public void setOptions(Options newOptions) {
        options = newOptions;
    }

    @Override
    public void start() {

        LOGGER.info("Запуск игры с настройками: высота = " + options.getRows() +
                ", ширина = " + options.getCols() +
                ", число мин = " +options.getNumberOfBombs());

        setUpOptions(options);
        view.restart(options.getRows(), options.getCols(), options.getNumberOfBombs());
    }

    @Override
    public void doMove(MoveType moveType, Cell cell) {

        LOGGER.info("Действие игрока: " + moveType +
                ", ячейка = (" + cell.getRow() + ", " + cell.getCol() + ")");

        clearUpdatingCells();
        processAction(moveType, cell);
        updateView();
    }


    @Override
    public void generateField(Cell startCell) {

        LOGGER.info("Генерация игрового поля.");

        field = generator.generate(startCell, options);
    }

    private void setUpOptions(Options newOptions) {

        options = newOptions;
        field = new Field(options.getRows(), options.getCols());
        isLose = false;
        cellsToUpdate = new ArrayList<>();
    }

    private boolean isEnd() {
        return isLose || field.getNumberOfOpenedCells() + options.getNumberOfBombs() ==
                options.getRows() * options.getCols();
    }


    private void processAction(MoveType moveType, Cell cell) {

        switch (moveType) {
            case OPEN:
                activateCell(cell);
                break;
            case OPEN_AROUND:
                activateNeighborsOfCell(cell);
                break;
            case FLAG:
                markCell(cell);
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }

    private void updateView() {

        if (!cellsToUpdate.isEmpty()) {
            view.updateCells(cellsToUpdate);
        }

        int numberOfRemainingBombsCount = options.getNumberOfBombs() - field.getNumberOfFlaggedCells();
        view.updateRemainingBombsCount(numberOfRemainingBombsCount);

        if (isEnd()) {

            String result = isLose ? "проиграл" : "победил";
            LOGGER.info("Игра завершилась: игрок " + result);

            view.handleEnd(!isLose);
        }
    }

    private void clearUpdatingCells() {
        cellsToUpdate.clear();
    }

    private void activateCell(Cell cell) {

        if (!field.isLocked(cell)) {
            return;
        }

        if (field.isBomb(cell)) {
            activateBomb(cell);
            isLose = true;
        } else {
            activateLocked(cell);
        }
    }

    private void activateNeighborsOfCell(Cell cell) {

        if (!field.isOpened(cell)) {
            return;
        }

        if (field.getNumberOfBombsAround(cell) > 0 &&
                field.getNumberOfBombsAround(cell) == numberOfNeighborFlags(cell)) {

            List<Cell> neighbors = field.getNeighbors(cell);
            for (Cell neighbor : neighbors) {
                if (field.isLocked(neighbor) && !isLose) {
                    activateCell(neighbor);
                }
            }
        }
    }

    private void markCell(Cell cell) {

        if (field.getCellType(cell) == CellView.FLAGGED) {
            removeFlag(cell);
        }
        else if (field.getCellType(cell) == CellView.LOCKED) {
            setUpFlag(cell);
        }
    }

    private void removeFlag(Cell cell) {

        field.setLock(cell);
        cellsToUpdate.add(new ViewCell(cell.getRow(), cell.getCol(), ViewCellType.LOCKED));
    }

    private void setUpFlag(Cell cell) {

        field.setFlag(cell);
        cellsToUpdate.add(new ViewCell(cell.getRow(), cell.getCol(), ViewCellType.FLAG));
    }

    private void activateBomb(Cell cell) {

        List<Cell> bombs = field.getBombs();

        for (Cell bomb : bombs) {
            ViewCellType bombType = bomb.equals(cell) ? ViewCellType.PRESSED_BOMB : ViewCellType.BOMB;
            cellsToUpdate.add(new ViewCell(bomb.getRow(), bomb.getCol(), bombType));
        }
    }

    private void activateLocked(Cell cell) {

        List<Cell> updatingCells = getCellsToUpdate(cell);

        for (Cell updatingCell : updatingCells) {
            cellsToUpdate.add(new ViewCell(
                    updatingCell.getRow(),
                    updatingCell.getCol(),
                    getViewCellTypeById(field.getNumberOfBombsAround(updatingCell))));
        }
    }

    private List<Cell> getCellsToUpdate(Cell startCell) {

        // Поиск пустых ячеек в ширину

        List<Cell> updatingCells = new ArrayList<>();
        Stack<Cell> activateCells = new Stack<>();
        activateCells.push(startCell);

        while (!activateCells.isEmpty()) {

            Cell currentCell = activateCells.pop();
            if (!field.isLocked(currentCell)) {
                continue;
            }
            field.setOpen(currentCell);
            updatingCells.add(currentCell);

            List<Cell> neighbors = field.getNeighbors(currentCell);
            for (Cell neighbor : neighbors) {
                if (field.getNumberOfBombsAround(currentCell) == 0 &&
                        field.isLocked(neighbor)) {
                    activateCells.push(neighbor);
                }
            }
        }

        return updatingCells;
    }

    private int numberOfNeighborFlags(Cell cell) {

        int numberOfBombs = 0;
        List<Cell> neighbors = field.getNeighbors(cell);

        for (Cell neighbor : neighbors) {
            if (field.isFlagged(neighbor)) {
                numberOfBombs++;
            }
        }

        return numberOfBombs;
    }

    private ViewCellType getViewCellTypeById(int id) {
        switch (id) {
            case 0: return ViewCellType.EMPTY;
            case 1: return ViewCellType.NEAR_BOMB_1;
            case 2: return ViewCellType.NEAR_BOMB_2;
            case 3: return ViewCellType.NEAR_BOMB_3;
            case 4: return ViewCellType.NEAR_BOMB_4;
            case 5: return ViewCellType.NEAR_BOMB_5;
            case 6: return ViewCellType.NEAR_BOMB_6;
            case 7: return ViewCellType.NEAR_BOMB_7;
            case 8: return ViewCellType.NEAR_BOMB_8;
            default:
                throw new UnsupportedOperationException();

        }
    }


    // для тестирования
    protected void setField(Field field) {
        this.field = field;
    }

    // для тестирования
    protected void setGenerator(Generator generator) {
        this.generator = generator;
    }
}
