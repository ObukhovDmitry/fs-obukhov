package ru.cft.model;

public class Record {

    private String userName;
    private int time;

    public Record(String userName, int time) {
        this.userName = userName;
        this.time = time;
    }


    public int getTime() {
        return time;
    }

    String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return time + " " + userName;
    }
}
