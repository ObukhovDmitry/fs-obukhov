package ru.cft.model;

/**
 * Интерфейс IRecords предоставляет набор действий для работы с рекордами
 */
public interface IRecords {

    /**
     * В этом методе обновляются рекорды
     * @param newRecord новые рекорды
     */
    void updateRecord(Record newRecord);

    /**
     * Этот метод предназначен для отображения рекордов
     */
    void showRecords();

    /**
     * Этот метод предназначен для проверки, является ли новое время рекордом
     * @param time время, которое претендует быть рекордом
     * @return  true - это рекорд,
     *          false - иначе
     */
    boolean isRecord(int time);
}
