package ru.cft.model;

public class Options {

    public static final Options EASY = new Options(9, 9, 10);
    public static final Options NORM = new Options(16, 16, 40);
    public static final Options HARD = new Options(16, 30, 99);

    private int rows = 9;
    private int cols = 9;
    private int numberOfBombs = 10;

    public Options(int rows, int cols, int numberOfBombs) {

        this.rows = rows;
        this.cols = cols;
        this.numberOfBombs = numberOfBombs;
    }

    int getRows() {
        return rows;
    }

    int getCols() {
        return cols;
    }

    int getNumberOfBombs() {
        return numberOfBombs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Options options = (Options) o;

        if (rows != options.rows) return false;
        if (cols != options.cols) return false;
        return numberOfBombs == options.numberOfBombs;
    }

    @Override
    public int hashCode() {
        int result = rows;
        result = 31 * result + cols;
        result = 31 * result + numberOfBombs;
        return result;
    }
}
