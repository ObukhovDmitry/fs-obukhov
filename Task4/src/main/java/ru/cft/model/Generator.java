package ru.cft.model;

import java.util.Random;

class Generator {

    Field generate(Cell startCell, Options options) {

        int rows = options.getRows();
        int cols = options.getCols();
        Field field = new Field(rows, cols);

        Random random = new Random(System.currentTimeMillis());
        int startCellId = startCell.getRow() * cols + startCell.getCol();
        int maxId = rows * cols - 1;

        for (int i = 0; i < options.getNumberOfBombs(); i++) {

            int row;
            int col;

            do {
                int cellId = random.nextInt(maxId);
                if (cellId >= startCellId) {
                    cellId++;
                }

                row = cellId / cols;
                col = cellId % cols;
            } while (field.isBomb(new Cell(row, col)));

            field.addBomb(row, col);
        }

        return field;
    }
}
