package ru.cft.model;

/**
 * Интерфейс IOptionsObserver предназначен для указания классу того, что он подписан на настройки игры
 * Поскольку моделей может быть несколько и все они могут зависеть от настроек игры, то все являются слушателями Options
 */
interface IOptionsObserver {

    /**
     * В этом методе наблюдатели реагируют на изменение настроек
     * @param newOptions новые настройки игры
     */
    void setOptions(Options newOptions);
}
