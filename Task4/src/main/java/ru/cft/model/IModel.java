package ru.cft.model;

import ru.cft.view.IView;

/**
 * Интерфейс IModel предназначен для указания классу того, что он является моделью MVC
 */
interface IModel {

    /**
     * В этом методе модели указывается с какой IView она будет работать
     * @param view представление, с которым будет работать модель
     */
    void setView(IView view);
}
