package ru.cft.model;

/**
 * Интерфейс IOptionsObservable определяет методы для добавления, удаления и оповещения наблюдателей
 * Поскольку моделей может быть несколько и все они могут зависеть от настроек игры, то все являются слушателями Options
 */
public interface IOptionsObservable {

    /**
     * В этом методе добавляются наблюдатели
     * @param observer наблюдатель
     */
    void addObserver(IOptionsObserver observer);

    /**
     * В этом методе оповещаются наблюдатели
     * @param newOptions настройки, которые надо применить
     */
    void notifyObservers(Options newOptions);
}
