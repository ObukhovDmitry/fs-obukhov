package ru.cft.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import ru.cft.controller.MoveHandler;

import java.util.EnumMap;
import java.util.Map;

import static ru.cft.view.Constants.*;

class ButtonsPane extends GridPane {

    private MoveHandler moveHandler;

    private int rows;
    private int cols;
    private Map<ViewCellType, String> imageUrlByCellType;
    private Button[][] buttons;

    ButtonsPane(MoveHandler moveHandler, int rows, int cols) {

        this.moveHandler = moveHandler;
        this.rows = rows;
        this.cols = cols;

        initImageUrls();
        initButtons();

        setPadding(new Insets(20));
    }

    void updateButton(ViewCell cell) {
        setButtonBackground(buttons[cell.getRow()][cell.getCol()], imageUrlByCellType.get(cell.getType()));
    }

    void deactivateAllButtons() {

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                deactivateButton(i, j);
            }
        }
    }

    private void deactivateButton(int row, int col) {
        buttons[row][col].setOnMouseClicked(e -> {});
    }

    private void initImageUrls() {

        imageUrlByCellType = new EnumMap<>(ViewCellType.class);
        imageUrlByCellType.put(ViewCellType.EMPTY, FILE_IMAGE_EMPTY);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_1, FILE_IMAGE_NUMBER_1);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_2, FILE_IMAGE_NUMBER_2);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_3, FILE_IMAGE_NUMBER_3);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_4, FILE_IMAGE_NUMBER_4);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_5, FILE_IMAGE_NUMBER_5);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_6, FILE_IMAGE_NUMBER_6);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_7, FILE_IMAGE_NUMBER_7);
        imageUrlByCellType.put(ViewCellType.NEAR_BOMB_8, FILE_IMAGE_NUMBER_8);
        imageUrlByCellType.put(ViewCellType.FLAG, FILE_IMAGE_FLAG);
        imageUrlByCellType.put(ViewCellType.BOMB, FILE_IMAGE_BOMB);
        imageUrlByCellType.put(ViewCellType.PRESSED_BOMB, FILE_IMAGE_PRESSED_BOMB);
        imageUrlByCellType.put(ViewCellType.LOCKED, FILE_IMAGE_NOT_PRESSED);
    }

    private void initButtons() {

        buttons = new Button[rows][cols];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                buttons[i][j] = getButton(i, j);
                setButtonBackground(buttons[i][j], imageUrlByCellType.get(ViewCellType.LOCKED));
            }
            addRow(i, buttons[i]);
        }

        setAlignment(Pos.CENTER);
    }

    private Button getButton(int row, int col) {

        Button button = new Button();

        button.setMinSize(VIEW_BUTTONS_SIZE, VIEW_BUTTONS_SIZE);
        button.setPrefSize(VIEW_BUTTONS_SIZE, VIEW_BUTTONS_SIZE);
        button.setMaxSize(VIEW_BUTTONS_SIZE, VIEW_BUTTONS_SIZE);

        button.setOnMouseClicked((event -> moveHandler.doMove(event, row, col)));

        return button;
    }

    private void setButtonBackground(Button button, String imageUrl) {

        BackgroundImage backgroundImage = new BackgroundImage(
                new Image(imageUrl),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT);
        Background background = new Background(backgroundImage);

        button.setBackground(background);
    }
}
