package ru.cft.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import static ru.cft.view.Constants.BUTTON_OK_TEXT;
import static ru.cft.view.Constants.FILE_IMAGE_ICON;

class AlertWindow {

    private static final int WINDOW_WIDTH = 250;
    private static final int WINDOW_HEIGHT = 80;

    void display(String title, String message) {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.getIcons().add(new Image(FILE_IMAGE_ICON));

        Label label = new Label();
        label.setText(message);

        Button button = new Button(BUTTON_OK_TEXT);
        button.setOnAction(e -> window.close());

        VBox layout = new VBox();
        layout.getChildren().addAll(label, button);
        layout.setAlignment(Pos.CENTER);

        window.setScene(new Scene(layout, WINDOW_WIDTH, WINDOW_HEIGHT));
        window.showAndWait();
    }
}
