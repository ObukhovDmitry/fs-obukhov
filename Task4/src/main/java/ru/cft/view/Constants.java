package ru.cft.view;

final class Constants {

    static final String GAME_NAME = "Сапер";
    static final String GAME_MODE_BEGINNER = "Новичек";
    static final String GAME_MODE_AMATEUR = "Любитель";
    static final String GAME_MODE_PROFESSIONAL = "Профессионал";

    static final int VIEW_BUTTONS_SIZE = 20;

    static final String MENU_GAME = "Игра";
    static final String MENU_HELP = "Справка";
    static final String MENU_ITEM_NEW_GAME = "Новая игра";
    static final String MENU_ITEM_BEGINNER = "Новичек";
    static final String MENU_ITEM_AMATEUR = "Любитель";
    static final String MENU_ITEM_PROFESSIONAL = "Профессионал";
    static final String MENU_ITEM_CUSTOM = "Особые...";
    static final String MENU_ITEM_RECORDS = "Рекорды";
    static final String MENU_ITEM_EXIT = "Выход";
    static final String MENU_ITEM_ABOUT = "О программе...";

    static final String BUTTON_OK_TEXT = "OK";
    static final String BUTTON_CANCEL_TEXT = "Отмена";

    static final String WINDOW_SETTINGS_TITLE = "Настройки";
    static final String SETTINGS_LABEL_HEIGHT = "Высота";
    static final String SETTINGS_LABEL_WIDTH = "Ширина";
    static final String SETTINGS_LABEL_MINES = "Число мин";
    static final String SETTINGS_TEXTFIELD_HEIGHT = "от 9 до 24";
    static final String SETTINGS_TEXTFIELD_WIDTH = "от 9 до 30";
    static final String SETTINGS_TEXTFIELD_MINES = "от 10";

    static final String WINDOW_NEW_RECORD_TITLE = "Чемпион";
    static final String NEW_RECORD_MESSAGE = "Поздравляем!\nВы установили рекорд\nВведите ваше имя:";

    static final String WINDOW_ABOUT_TITLE = "О программе";
    static final String ABOUT_MESSAGE = "Задача 4. Игра \"Сапер\"";

    static final String WINDOW_RECORD_TITLE = "Рекорды";

    static final String WINDOW_WIN_TITLE = "Победа";
    static final String WIN_MESSAGE_1 = "Вы победили! \nВаше время ";
    static final String WIN_MESSAGE_2 =  " секунд";

    static final String WINDOW_LOSE_TITLE = "Поражение";
    static final String LOSE_MESSAGE = "Вы проиграли!";

    static final String FILE_IMAGE_ICON = "icon.png";
    static final String FILE_IMAGE_EMPTY = "empty.png";
    static final String FILE_IMAGE_NUMBER_1 = "1.png";
    static final String FILE_IMAGE_NUMBER_2 = "2.png";
    static final String FILE_IMAGE_NUMBER_3 = "3.png";
    static final String FILE_IMAGE_NUMBER_4 = "4.png";
    static final String FILE_IMAGE_NUMBER_5 = "5.png";
    static final String FILE_IMAGE_NUMBER_6 = "6.png";
    static final String FILE_IMAGE_NUMBER_7 = "7.png";
    static final String FILE_IMAGE_NUMBER_8 = "8.png";
    static final String FILE_IMAGE_FLAG = "flag.png";
    static final String FILE_IMAGE_BOMB = "bomb.png";
    static final String FILE_IMAGE_PRESSED_BOMB = "pressed_bomb.png";
    static final String FILE_IMAGE_NOT_PRESSED = "not_pressed.png";

    private Constants() {}
}
