package ru.cft.view;

public class ViewCell {

    private int row;
    private int col;
    private ViewCellType type;

    public ViewCell(int row, int col, ViewCellType type) {

        this.row = row;
        this.col = col;
        this.type = type;
    }

    int getRow() {
        return row;
    }

    int getCol() {
        return col;
    }

    ViewCellType getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewCell viewCell = (ViewCell) o;

        if (row != viewCell.row) return false;
        if (col != viewCell.col) return false;
        return type == viewCell.type;
    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + col;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
