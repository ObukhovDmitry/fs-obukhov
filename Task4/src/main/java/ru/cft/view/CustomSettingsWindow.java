package ru.cft.view;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.cft.controller.GameHandler;

import static ru.cft.view.Constants.*;

class CustomSettingsWindow {

    private static final int WINDOW_WIDTH = 250;
    private static final int WINDOW_HEIGHT = 150;

    void display(GameHandler gameHandler) {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(WINDOW_SETTINGS_TITLE);
        window.getIcons().add(new Image(FILE_IMAGE_ICON));

        Label labelRows = new Label();
        labelRows.setText(SETTINGS_LABEL_HEIGHT);

        Label labelCols = new Label();
        labelCols.setText(SETTINGS_LABEL_WIDTH);

        Label labelMines = new Label();
        labelMines.setText(SETTINGS_LABEL_MINES);

        TextField textAreaRows = new TextField();
        textAreaRows.setPromptText(SETTINGS_TEXTFIELD_HEIGHT);

        TextField textAreaCols = new TextField();
        textAreaCols.setPromptText(SETTINGS_TEXTFIELD_WIDTH);

        TextField textAreaMines = new TextField();
        textAreaMines.setPromptText(SETTINGS_TEXTFIELD_MINES);

        Button buttonOk = new Button(BUTTON_OK_TEXT);
        buttonOk.setOnAction(event ->  {
            try {
                int rows = Integer.parseInt(textAreaRows.getText());
                int cols = Integer.parseInt(textAreaCols.getText());
                int mines = Integer.parseInt(textAreaMines.getText());

                gameHandler.restart(rows, cols, mines);

            } catch (NumberFormatException ignore) {
            } finally {
                window.close();
            }
        });

        Button buttonCancel = new Button(BUTTON_CANCEL_TEXT);
        buttonCancel.setOnAction(e -> window.close());

        GridPane gridPane = new GridPane();
        gridPane.addRow(0, labelRows, textAreaRows);
        gridPane.addRow(1, labelCols, textAreaCols);
        gridPane.addRow(2, labelMines, textAreaMines);
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        AnchorPane buttonsPane = new AnchorPane();
        buttonsPane.getChildren().addAll(buttonOk, buttonCancel);

        AnchorPane.setBottomAnchor(buttonOk, 10.0);
        AnchorPane.setLeftAnchor(buttonOk, 20.0);
        AnchorPane.setBottomAnchor(buttonCancel, 10.0);
        AnchorPane.setRightAnchor(buttonCancel, 20.0);

        BorderPane basePane = new BorderPane();
        basePane.setCenter(gridPane);
        basePane.setBottom(buttonsPane);

        window.setScene(new Scene(basePane, WINDOW_WIDTH, WINDOW_HEIGHT));
        window.showAndWait();
    }
}
