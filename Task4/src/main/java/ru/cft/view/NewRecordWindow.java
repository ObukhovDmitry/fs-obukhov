package ru.cft.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.cft.controller.RecordsHandler;

import static ru.cft.view.Constants.*;

class NewRecordWindow {

    private static final int WINDOW_WIDTH = 250;
    private static final int WINDOW_HEIGHT = 140;
    private static final int WINDOW_PADDING = 15;
    private static final int WINDOW_SPACING = 5;

    void display(RecordsHandler recordsHandler, long time) {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(WINDOW_NEW_RECORD_TITLE);
        window.getIcons().add(new Image(FILE_IMAGE_ICON));

        Label message = new Label(NEW_RECORD_MESSAGE);

        TextField inputForm = new TextField();
        inputForm.setPrefWidth(100);

        Button button = new Button(BUTTON_OK_TEXT);
        button.setOnAction(e -> {
            recordsHandler.updateRecord(inputForm.getText(), (int) time);
            window.close();
        });

        VBox layout = new VBox();
        layout.getChildren().addAll(message, inputForm, button);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(WINDOW_PADDING));
        layout.setSpacing(WINDOW_SPACING);

        window.setScene(new Scene(layout, WINDOW_WIDTH, WINDOW_HEIGHT));
        window.showAndWait();
    }
}
