package ru.cft.view;

public class ViewRecord {

    private String userName;
    private int time;

    public ViewRecord(String userName, int time) {
        this.userName = userName;
        this.time = time;
    }

    @Override
    public String toString() {
        return time + " " + userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ViewRecord record = (ViewRecord) o;

        if (time != record.time) return false;
        return userName != null ? userName.equals(record.userName) : record.userName == null;
    }

    @Override
    public int hashCode() {
        int result = userName != null ? userName.hashCode() : 0;
        result = 31 * result + time;
        return result;
    }
}
