package ru.cft.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import static ru.cft.view.Constants.*;

class RecordsWindow {

    private static final int WINDOW_WIDTH = 250;
    private static final int WINDOW_HEIGHT = 110;
    private static final int WINDOW_SPACING = 5;

    void display(List<ViewRecord> records) {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(WINDOW_RECORD_TITLE);
        window.getIcons().add(new Image(FILE_IMAGE_ICON));

        List<String> recordModeStrings = new ArrayList<>();
        recordModeStrings.add(GAME_MODE_BEGINNER);
        recordModeStrings.add(GAME_MODE_AMATEUR);
        recordModeStrings.add(GAME_MODE_PROFESSIONAL);

        Button button = new Button(BUTTON_OK_TEXT);
        button.setOnAction(e -> window.close());

        GridPane textPane = new GridPane();
        for (int i = 0; i < records.size(); i++) {
            Label recordModeLabel = new Label(recordModeStrings.get(i));
            Label recordValueLabel = new Label(records.get(i).toString());
            textPane.addRow(i, recordModeLabel, recordValueLabel);
        }
        textPane.setVgap(WINDOW_SPACING);
        textPane.setHgap(WINDOW_SPACING);
        textPane.setPadding(new Insets(WINDOW_SPACING));
        textPane.setAlignment(Pos.CENTER);

        VBox basePane = new VBox();
        basePane.getChildren().addAll(textPane, button);
        basePane.setAlignment(Pos.CENTER);

        window.setScene(new Scene(basePane, WINDOW_WIDTH, WINDOW_HEIGHT));
        window.showAndWait();
    }
}
