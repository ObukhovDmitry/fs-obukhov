package ru.cft.view;

import ru.cft.controller.GameHandler;
import ru.cft.controller.MoveHandler;
import ru.cft.controller.RecordsHandler;

import java.util.List;

/**
 * Интерфейс IView предназначен для взаимодействия с моделью
 */
public interface IView {

    /**
     * Перезапуск игры, что означает перерисовку игрового окна
     * @param rows число ячеек по вертикали
     * @param cols число ячеек по горизонтали
     * @param numberOfMines число мин
     */
    void restart(int rows, int cols, int numberOfMines);

    /**
     * Установка контроллера для работы с запуском и перезапуском игры
     * @param gameHandler контроллер
     */
    void setGameHandler(GameHandler gameHandler);

    /**
     * Установка контроллера, который реагирует на ходы пользователя
     * @param moveHandler контроллер
     */
    void setMoveHandler(MoveHandler moveHandler);

    /**
     * Установка контроллера, который работает с рекордами
     * @param recordsHandler контроллер
     */
    void setRecordsHandler(RecordsHandler recordsHandler);

    /**
     * В этом методе происходит обновление ячеек
     * ViewCell содержит информация о ячейки, которую надо обновить, необходимую для представления
     * @param updatingCells список ячеек, которые надо обновить
     */
    void updateCells(List<ViewCell> updatingCells);

    /**
     * В этом методе происходит обновление числа мин, которые осталось найти
     * @param numberOfRemainingBombs число мин, которые осталось найти
     */
    void updateRemainingBombsCount(int numberOfRemainingBombs);

    /**
     * В этом методе происходи обновление таймера
     * @param time новое время
     */
    void updateTimer(int time);

    /**
     * Этот метод предназначен для обработки конца игры
     * @param isWin true - если результат победа;
     *              false - если результат поражение
     */
    void handleEnd(boolean isWin);

    /**
     * В этом методе рекорды отображаются пользователю
     * @param records рекорды
     */
    void showRecords(List<ViewRecord> records);
}
