package ru.cft.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import ru.cft.controller.GameHandler;
import ru.cft.controller.RecordsHandler;

import static ru.cft.view.Constants.*;

class MenuPane extends MenuBar {

    private GameHandler gameHandler;
    private RecordsHandler recordsHandler;
    private Menu menuGame;
    private Menu menuHelp;
    private AlertWindow alertWindow;
    private CustomSettingsWindow customSettingsWindow;

    MenuPane(GameHandler gameHandler, RecordsHandler recordsHandler) {

        this.gameHandler = gameHandler;
        this.recordsHandler = recordsHandler;
        alertWindow = new AlertWindow();
        customSettingsWindow = new CustomSettingsWindow();

        initMenuGame();
        initMenuHelp();

        getMenus().addAll(menuGame, menuHelp);
    }

    private void initMenuGame() {

        menuGame = new Menu(MENU_GAME);

        MenuItem newGame = new MenuItem(MENU_ITEM_NEW_GAME);
        MenuItem beginner = new MenuItem(MENU_ITEM_BEGINNER);
        MenuItem amateur = new MenuItem(MENU_ITEM_AMATEUR);
        MenuItem professional = new MenuItem(MENU_ITEM_PROFESSIONAL);
        MenuItem custom = new MenuItem(MENU_ITEM_CUSTOM);
        MenuItem records = new MenuItem(MENU_ITEM_RECORDS);
        MenuItem exit = new MenuItem(MENU_ITEM_EXIT);

        newGame     .setOnAction(event -> gameHandler.restart());
        beginner    .setOnAction(event -> gameHandler.restartEasy());
        amateur     .setOnAction(event -> gameHandler.restartNorm());
        professional.setOnAction(event -> gameHandler.restartHard());
        custom      .setOnAction(event -> customSettingsWindow.display(gameHandler));
        records     .setOnAction(event -> recordsHandler.showRecords());
        exit        .setOnAction(event -> System.exit(0));

        menuGame.getItems().addAll(newGame,
                new SeparatorMenuItem(), beginner, amateur, professional, custom,
                new SeparatorMenuItem(), records,
                new SeparatorMenuItem(), exit);
    }

    private void initMenuHelp() {

        menuHelp = new Menu(MENU_HELP);
        MenuItem about = new MenuItem(MENU_ITEM_ABOUT);
        about.setOnAction(e -> alertWindow.display(WINDOW_ABOUT_TITLE, ABOUT_MESSAGE));
        menuHelp.getItems().add(about);
    }
}
