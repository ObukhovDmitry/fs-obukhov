package ru.cft.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

class InformationPane extends StackPane {

    private Label numberOfRemainingBombs;
    private TimerPane timerPane;

    InformationPane(Integer numberOfBombs) {

        timerPane = new TimerPane();

        numberOfRemainingBombs = new Label();
        numberOfRemainingBombs.setText(numberOfBombs.toString());
        numberOfRemainingBombs.setFont(new Font("Tahoma", 24));
        numberOfRemainingBombs.setStyle("-fx-border-color:darkgrey; -fx-background-color: lightgray;");

        getChildren().addAll(timerPane, numberOfRemainingBombs);
        setPadding(new Insets(10, 10, 10, 10));
        setAlignment(timerPane, Pos.CENTER_LEFT);
        setAlignment(numberOfRemainingBombs, Pos.CENTER_RIGHT);
    }

    void updateRemainingBombsCount(int numberOfRemainingBombs) {

        Integer notNegativeNumberOfRemainingBombs = Math.max(numberOfRemainingBombs, 0);
        this.numberOfRemainingBombs.setText(notNegativeNumberOfRemainingBombs.toString());
    }

    void updateTimer(int time) {
        timerPane.updateTime(time);
    }
}
