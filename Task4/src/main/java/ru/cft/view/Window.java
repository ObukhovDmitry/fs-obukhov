package ru.cft.view;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ru.cft.controller.GameHandler;
import ru.cft.controller.MoveHandler;
import ru.cft.controller.RecordsHandler;

import java.util.List;

import static ru.cft.view.Constants.*;

public class Window implements IView {

    private GameHandler gameHandler;
    private MoveHandler moveHandler;
    private RecordsHandler recordsHandler;

    private Stage primaryStage;
    private ButtonsPane buttonPane;
    private InformationPane informationPane;
    private MenuPane menuPane;
    private AlertWindow alertWindow;

    private int time;
    private NewRecordWindow newRecordWindow;
    private RecordsWindow recordsWindow;

    public Window(Stage primaryStage) {

        initPrimaryStage(primaryStage);

        alertWindow = new AlertWindow();
        newRecordWindow = new NewRecordWindow();
        recordsWindow = new RecordsWindow();
    }

    @Override
    public void setGameHandler(GameHandler gameHandler) {
        this.gameHandler = gameHandler;
    }

    @Override
    public void setMoveHandler(MoveHandler moveHandler) {
        this.moveHandler = moveHandler;
    }

    @Override
    public void setRecordsHandler(RecordsHandler recordsHandler) {
        this.recordsHandler = recordsHandler;
    }

    @Override
    public void restart(int rows, int cols, int numberOfBombs) {

        moveHandler.endGame();

        buttonPane = new ButtonsPane(moveHandler, rows, cols);
        informationPane = new InformationPane(numberOfBombs);
        menuPane = new MenuPane(gameHandler, recordsHandler);

        BorderPane rootPane = new BorderPane();
        rootPane.setTop(menuPane);
        rootPane.setCenter(buttonPane);
        rootPane.setBottom(informationPane);

        primaryStage.setScene(new Scene(rootPane));
        primaryStage.show();

    }

    @Override
    public void updateCells(List<ViewCell> updatingCells) {

        for (ViewCell cell : updatingCells) {
            buttonPane.updateButton(cell);
        }
    }

    @Override
    public void updateRemainingBombsCount(int numberOfRemainingBombs) {
        informationPane.updateRemainingBombsCount(numberOfRemainingBombs);
    }

    @Override
    public void updateTimer(int time) {

        this.time = time;
        informationPane.updateTimer(time);
    }

    @Override
    public void handleEnd(boolean isWin) {

        moveHandler.endGame();

        if (isWin && recordsHandler.isRecord(time)) {
            handleRecordWin();
        } else if (isWin) {
            handleWin();
        } else {
            handleLose();
        }
    }

    @Override
    public void showRecords(List<ViewRecord> records) {
        recordsWindow.display(records);
    }

    private void initPrimaryStage(Stage stage) {

        primaryStage = stage;
        primaryStage.setTitle(GAME_NAME);
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(event -> Platform.exit());
        primaryStage.getIcons().add(new Image(FILE_IMAGE_ICON));
    }

    private void handleRecordWin() {

        newRecordWindow.display(recordsHandler, time);
        buttonPane.deactivateAllButtons();
    }

    private void handleWin() {

        alertWindow.display(WINDOW_WIN_TITLE, WIN_MESSAGE_1 + time + WIN_MESSAGE_2);
        buttonPane.deactivateAllButtons();
    }

    private void handleLose() {

        alertWindow.display(WINDOW_LOSE_TITLE, LOSE_MESSAGE);
        buttonPane.deactivateAllButtons();
    }
}
