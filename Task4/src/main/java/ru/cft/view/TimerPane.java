package ru.cft.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

class TimerPane extends StackPane {

    private Label timeLabel;

    TimerPane() {

        timeLabel = new Label();
        timeLabel.setText("0");
        timeLabel.setFont(new Font("Tahoma", 24));
        timeLabel.setStyle("-fx-border-color:darkgrey; -fx-background-color: lightgray;");

        getChildren().add(timeLabel);
        setPadding(new Insets(10, 10, 10, 10));
        setAlignment(timeLabel, Pos.CENTER_LEFT);
    }

    void updateTime(int time) {
        timeLabel.setText(Integer.toString(time));
    }
}
