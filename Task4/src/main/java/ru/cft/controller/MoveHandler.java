package ru.cft.controller;

import javafx.event.Event;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import ru.cft.model.Cell;
import ru.cft.model.IGame;
import ru.cft.model.MoveType;

import java.util.EnumMap;
import java.util.Map;

public class MoveHandler {

    private IGame game;
    private Map<MouseButton, MoveType> moveTypeMap;
    private TimerHandler timerHandler;
    private boolean isFirstMove = true;

    public void setGame(IGame game) {
        this.game = game;
    }

    public void setTimerHandler(TimerHandler timer) {
        this.timerHandler = timer;
    }

    public MoveHandler() {

        moveTypeMap = new EnumMap<>(MouseButton.class);
        moveTypeMap.put(MouseButton.PRIMARY, MoveType.OPEN);
        moveTypeMap.put(MouseButton.SECONDARY, MoveType.FLAG);
        moveTypeMap.put(MouseButton.MIDDLE, MoveType.OPEN_AROUND);
    }

    public void doMove(Event event, int row, int col) {

        if (event.getClass().equals(MouseEvent.class)) {

            MouseEvent mouseEvent = (MouseEvent) event;
            MoveType moveType = moveTypeMap.get(mouseEvent.getButton());

            if (isFirstMove) {

                if (moveType != MoveType.OPEN) {
                    return;
                }

                isFirstMove = false;
                timerHandler.start();
                game.generateField(new Cell(row, col));

            }

            game.doMove(moveType, new Cell(row, col));
        }
    }

    public void endGame() {

        isFirstMove = true;
        if (timerHandler != null && timerHandler.isTimerStarted()) {
            timerHandler.stop();
        }
    }
}
