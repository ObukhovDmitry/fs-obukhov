package ru.cft.controller;

import ru.cft.model.IRecords;
import ru.cft.model.Record;

public class RecordsHandler {

    private IRecords records;

    public void setRecords(IRecords records) {
        this.records = records;
    }

    public void showRecords() {
        records.showRecords();
    }

    public void updateRecord(String userName, int time) {
        records.updateRecord(new Record(userName, time));
    }

    public boolean isRecord(int time) {
        return records.isRecord(time);
    }
}
