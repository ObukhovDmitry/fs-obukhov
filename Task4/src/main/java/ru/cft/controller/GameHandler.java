package ru.cft.controller;

import ru.cft.model.IOptionsObservable;
import ru.cft.model.Options;
import ru.cft.model.IGame;

public class GameHandler {

    private IGame game;
    private IOptionsObservable optionsObservable;

    public void setGame(IGame game) {
        this.game = game;
    }

    public void setOptionsObservable(IOptionsObservable optionsObservable) {
        this.optionsObservable = optionsObservable;
    }

    public void restart() {
        game.start();
    }

    public void restartEasy() {
        optionsObservable.notifyObservers(Options.EASY);
        restart();
    }

    public void restartNorm() {
        optionsObservable.notifyObservers(Options.NORM);
        restart();
    }

    public void restartHard() {
        optionsObservable.notifyObservers(Options.HARD);
        restart();
    }

    public void restart(int rows, int cols, int numberOfMines) {

        if (!checkRows(rows) || !checkCols(cols) || !checkNumberOfMines(rows, cols, numberOfMines)) {
            return;
        }
        optionsObservable.notifyObservers(new Options(rows, cols, numberOfMines));
        restart();
    }

    private boolean checkRows(int rows) {
        return rows >= 9 && rows <= 24;
    }

    private boolean checkCols(int cols) {
        return cols >= 9 && cols <= 30;
    }

    private boolean checkNumberOfMines(int rows, int cols, int mines) {
        return mines >= 10 && mines <= (rows - 1) * (cols - 1);
    }
}
