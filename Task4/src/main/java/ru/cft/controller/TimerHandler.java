package ru.cft.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.model.GameTime;

public class TimerHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimerHandler.class);

    private GameTime gameTime;
    private Timeline timerTask;
    private boolean isStarted;

    public void setGameTime(GameTime gameTime) {
        this.gameTime = gameTime;
    }

    void start() {

        LOGGER.debug("Таймер запущен.");

        gameTime.setStartTime(System.currentTimeMillis());
        isStarted = true;
        timerTask = new Timeline(new KeyFrame(Duration.seconds(1),
                event ->  gameTime.updateTime(getTimeInSeconds())));
        timerTask.setCycleCount(999);
        timerTask.play();
    }

    void stop() {

        LOGGER.debug("Таймер остановлен. Время игры = {}", getTimeInSeconds());

        isStarted = false;
        timerTask.stop();
    }

    boolean isTimerStarted() {
        return isStarted;
    }

    private int getTimeInSeconds() {

        return isStarted ?
                (int) (System.currentTimeMillis() - gameTime.getStartTime()) / 1000 :
                (int) (gameTime.getStartTime() / 1000);
    }
}
