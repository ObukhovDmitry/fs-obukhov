package ru.cft;

import javafx.application.Application;
import javafx.stage.Stage;

import ru.cft.dao.RecordDao;
import ru.cft.model.*;
import ru.cft.controller.*;
import ru.cft.view.Window;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        GameHandler gameHandler = new GameHandler();
        MoveHandler moveHandler = new MoveHandler();
        RecordsHandler recordsHandler = new RecordsHandler();
        TimerHandler timerHandler = new TimerHandler();
        moveHandler.setTimerHandler(timerHandler);

        Window window = new Window(primaryStage);
        window.setGameHandler(gameHandler);
        window.setMoveHandler(moveHandler);
        window.setRecordsHandler(recordsHandler);

        Sapper game = new Sapper();
        game.setView(window);
        gameHandler.setGame(game);
        moveHandler.setGame(game);

        Records records = new Records(new RecordDao());
        records.setView(window);
        recordsHandler.setRecords(records);

        GameTime gameTime = new GameTime();
        gameTime.setView(window);
        timerHandler.setGameTime(gameTime);


        IOptionsObservable optionsObservable = new OptionsObservable();
        optionsObservable.addObserver(game);
        optionsObservable.addObserver(records);
        optionsObservable.notifyObservers(Options.EASY);
        gameHandler.setOptionsObservable(optionsObservable);

        game.start();
    }
}
