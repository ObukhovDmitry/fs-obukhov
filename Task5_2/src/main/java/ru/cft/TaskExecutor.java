package ru.cft;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class TaskExecutor {

    private List<Future<Double>> futures;
    private SomeFunction function;
    private int numberOfThreads;
    private int numberOfTerms;

    TaskExecutor(SomeFunction function, int numberOfThreads, int numberOfTerms) {

        this.function = function;
        this.numberOfThreads = numberOfThreads;
        this.numberOfTerms = numberOfTerms;
    }

    void submit() {

        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);
        futures = new ArrayList<>(numberOfThreads);

        for (int i = 0; i < numberOfThreads; i++) {
            ArgumentIterator currentThreadArgumentIterator = new ArgumentIterator(i, numberOfTerms, numberOfThreads);
            Task currentThreadTask = new Task(function, currentThreadArgumentIterator);
            Future<Double> currentThreadFuture = executorService.submit(currentThreadTask);

            futures.add(currentThreadFuture);
        }

        executorService.shutdown();
    }

    double getResult() {

        return futures.stream()
                .mapToDouble(future -> {
                    try {
                        return future.get();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        return 0;
                    }
                })
                .sum();
    }
}
