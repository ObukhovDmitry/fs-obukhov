package ru.cft;

class ArgumentIterator {

    private int currentIndex;
    private int maxIndex;
    private int growth;

    ArgumentIterator(int startIndex, int maxIndex, int growth) {

        this.currentIndex = startIndex - growth;
        this.maxIndex = maxIndex;
        this.growth = growth;
    }

    boolean hasNext() {
        return currentIndex + growth < maxIndex;
    }

    int next() {

        currentIndex += growth;
        return currentIndex;
    }
}
