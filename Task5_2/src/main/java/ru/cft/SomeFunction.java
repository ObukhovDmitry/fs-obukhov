package ru.cft;

class SomeFunction {
    Double compute(Integer n) {
        return Math.pow(2, -n);
    }
}
