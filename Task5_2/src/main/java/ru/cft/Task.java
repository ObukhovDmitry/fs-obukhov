package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

class Task implements Callable<Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Task.class);

    private SomeFunction function;
    private ArgumentIterator argumentIterator;

    Task(SomeFunction function, ArgumentIterator argumentIterator) {

        this.function = function;
        this.argumentIterator = argumentIterator;
    }

    @Override
    public Double call() throws Exception {

        LOGGER.info("Calculation started");
        double result = 0;

        while (argumentIterator.hasNext()) {
            result += function.compute(argumentIterator.next());
        }
        LOGGER.info("Calculation finished, result = {}", result);

        return result;
    }
}
