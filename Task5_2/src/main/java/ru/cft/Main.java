package ru.cft;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        SomeFunction function = new SomeFunction();
        int numberOfThreads = 4;
        int numberOfTerms = 10000000;

        TaskExecutor taskExecutor = new TaskExecutor(function, numberOfThreads, numberOfTerms);
        taskExecutor.submit();

        LOGGER.info("Result of the sum: {}", taskExecutor.getResult());
    }
}
