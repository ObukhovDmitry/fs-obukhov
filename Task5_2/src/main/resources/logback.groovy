import ch.qos.logback.classic.filter.ThresholdFilter

appender("CONSOLE", ConsoleAppender) {
    target = "System.out"
    filter(ThresholdFilter) {
        level = INFO
    }
    encoder(PatternLayoutEncoder) {
        pattern = "[%thread] %d{HH:mm:ss.SSS} %msg%n"
    }
}

appender("FILE", FileAppender) {
    file = "Task5_2/src/main/resources/task5_2.log"
    append = false
    filter(ThresholdFilter) {
        level = DEBUG
    }
    encoder(PatternLayoutEncoder) {
        pattern = "[%thread] %logger %-5level %d{HH:mm:ss.SSS} - %msg%n"
    }
}

root(DEBUG, ["CONSOLE", "FILE"])