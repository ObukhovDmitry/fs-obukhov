package ru.cft.server;

import ru.cft.common.command.ConsoleQueryQueue;

class ServerCommandQueue extends ConsoleQueryQueue<ServerCommand> {

    @Override
    protected ServerCommand getQueryByString(String line) {
        return ServerCommand.getByConsoleName(line.toLowerCase());
    }
}
