package ru.cft.server;

import ru.cft.common.command.IQuery;

import java.util.HashMap;
import java.util.Map;

/**
 * ServerCommand - перечисление команд, которые можно выполнять над сервером
 */
enum ServerCommand implements IQuery {

    /**
     * START - запуск сервера
     */
    START ("/start", "\t/start\tServer startup\n"),

    /**
     * STOP - остоновка сервера
     */
    STOP ("/stop", "\t/stop\tServer shutdown\n"),

    /**
     * PAUSE - приостоновка сервера (сокеты клиетов не удаляются, clientPool не очищается)
     */
    PAUSE ("/pause", "\t/pause\tServer pause\n"),

    /**
     * STATUS - текущее состояние сервера (запущен или нет)
     */
    STATUS ("/status", "\t/status\tCheck server status\n"),

    /**
     * HELP - список команд и их краткое описание
     */
    HELP ("/help", "\t/help\tCommand list\n"),

    /**
     * CLIENT_OFF - остановка сервера и выход из программы
     */
    EXIT ("/exit", "\t/exit\tServer shutdown and exit");

    /**
     * command - имя команды в консоли
     */
    private String consoleName;

    /**
     * description - описание команды
     */
    private String description;

    /**
     * commandByConsoleName - мапа для получения команды по её названию в консоли
     */
    private static final Map<String, ServerCommand> commandByConsoleName = new HashMap<>();

    /*
     * Статичный блок для инициализации commandByConsoleName
     */
    static {
        for (ServerCommand command : ServerCommand.values()) {
            commandByConsoleName.put(command.consoleName, command);
        }
    }

    /**
     * Конструктор
     * @param consoleName имя команды в консоли
     * @param description описание команды
     */
    ServerCommand(String consoleName, String description) {

        this.consoleName = consoleName;
        this.description = description;
    }

    /**
     * description - описание команды
     */
    String getDescription() {
        return description;
    }

    /**
     * commandByConsoleName - мапа для получения команды по её названию в консоли
     */
    static ServerCommand getByConsoleName(String name) {
        return commandByConsoleName.get(name);
    }
}
