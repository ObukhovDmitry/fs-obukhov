package ru.cft.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

class Server implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private ServerSocket serverSocket;
    private ClientsPool clients;
    private int port;
    private boolean isStarted;

    Server(int port) {

        this.port = port;
        clients = new ClientsPool();
    }

    @Override
    public void run() {

        runServer();
        acceptClients();
    }

    private void runServer() {

        try {
            serverSocket = new ServerSocket(port);
            isStarted = true;
            LOGGER.info("Server running on the port {}.", port);

        } catch (IOException e) {
            LOGGER.error("Unable to connect to start the server on port {}.", port);
        }
    }

    private void acceptClients() {

        while (isStarted) {
            try {
                Socket clientSocket = serverSocket.accept();
                ClientListener clientListener = new ClientListener(clientSocket, clients);
                new Thread(clientListener).start();

            } catch (IOException e) {
                LOGGER.error("Thread of server socket is interrupted.");
            }
        }
    }

    void end() {
        try {
            serverSocket.close();
            isStarted = false;
            clients.notifyEnd();
            LOGGER.info("Server is stopped.");

        } catch (IOException e) {
            LOGGER.error("Unable to close server.", e);
        }
    }

    boolean isStarted() {
        return isStarted;
    }
}
