package ru.cft.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.cft.common.message.Message;
import ru.cft.common.message.MessageType;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class ClientListener implements Runnable {

    private static final Object SYNC = new Object();
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientListener.class);
    private static final String FILE_SCHEMA = "message.xsd";
    private static final String PATTERN_STRING = "<\\?xml(.*?)</message>";
    private static final long THREAD_SLEEP_MILLS = 100;
    private static long nextId = 0;

    private Unmarshaller unmarshaller;
    private Marshaller marshaller;

    private ClientPoolObservable clientsPool;
    private Socket clientSocket;
    private String clientName;
    private long id;

    ClientListener(Socket clientSocket, ClientPoolObservable clientsPool) {

        synchronized (SYNC) {
            id = nextId++;
        }

        this.clientSocket = clientSocket;
        this.clientsPool = clientsPool;

        try {
            JAXBContext context = JAXBContext.newInstance(Message.class);
            unmarshaller = context.createUnmarshaller();
            marshaller = context.createMarshaller();

            ClassLoader classLoader = getClass().getClassLoader();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(classLoader.getResource(FILE_SCHEMA).getFile()));
            unmarshaller.setSchema(schema);
            marshaller.setSchema(schema);

        } catch (JAXBException e) {
            LOGGER.error("JAXB error. ", e);
        } catch (SAXException e) {
            LOGGER.error("SAX error. ", e);
        }
    }

    @Override
    public void run() {

        while(!clientSocket.isClosed()) {
            try {
                if (clientSocket.getInputStream().available() > 0) {
                    LOGGER.debug("Client #{} ({}) is sends message.", id, clientName);

                    List<Message> messages = readMessage(clientSocket.getInputStream());
                    messages.forEach(this::processMessage);

                } else {
                    Thread.sleep(THREAD_SLEEP_MILLS);
                }

            } catch (IOException e) {
                LOGGER.info("Client socket is closed.");
            } catch (JAXBException e) {
                LOGGER.info("Unexpected message format.");
            } catch (InterruptedException e) {
                LOGGER.info("Client thread is interrupted.");
            }
        }

        LOGGER.info("Client #{} ({}) disconnected.", id, clientName);
    }

    void sendMessage(Message message) {

        try {
            marshaller.marshal(message, clientSocket.getOutputStream());
            LOGGER.debug("Send {} message to client #{} ({}).", message.getType(), id, clientName);

        } catch (IOException | JAXBException e) {
            LOGGER.info("Server can't resend message to {}", clientName);

            closeConnection();

            Message disconnectMessage = new Message();
            disconnectMessage.setType(MessageType.CLIENT_OFF);
            disconnectMessage.setLogin(clientName);
            clientsPool.notifyClientHandlers(disconnectMessage);
        }
    }

    void closeConnection() {

        try {
            clientSocket.close();
        } catch (IOException e) {
            LOGGER.info("Client socket can't be closed.");
        }
        clientsPool.delete(clientName);
    }

    private void processMessage(Message message) {

        if (isLoginMessageFromNewClient(message)) {
            processNewClient(message);

        } else if (isMessageFromKnownUser(message)) {
            processMessageFromKnownUser(message);
        }
    }

    private boolean isLoginMessageFromNewClient(Message message) {
        return clientName == null && message.getType() == MessageType.LOGIN;
    }

    private boolean isMessageFromKnownUser(Message message) {
        return clientName != null && message.getType() != MessageType.LOGIN;
    }

    private void processNewClient(Message message) {

        clientName = message.getLogin();
        checkClientPoolConnections();

        if (clientsPool.addClientHandler(clientName, this)) {

            LOGGER.info("Client #{} ({}) connected.", id, clientName);
            notifyPoolAboutNewClient();

        } else {

            LOGGER.debug("Client #{} cant't be connected, reason: name {} is used.", id, clientName);
            sendRejectMessage();
            clientName = null;
            try {
                clientSocket.close();
            } catch (IOException e) {
                LOGGER.error("Client socket can't be closed. ", e);
            }
            LOGGER.debug("Client socket #{} is closed.", id);
        }
    }

    private void processMessageFromKnownUser(Message message) {

        switch (message.getType()) {

            case LOGIN:
                throw new UnsupportedOperationException();

            case REJECT:
                throw new UnsupportedOperationException();

            case TEXT:
                message.setLogin(clientName);
                clientsPool.notifyClientHandlers(message);
                break;

            case USER_LIST:
                checkClientPoolConnections();
                message.setText(String.join(", ", clientsPool.getClientsNames()));
                sendMessage(message);
                break;

            case CLIENT_OFF:
                closeConnection();
                clientsPool.notifyClientHandlers(message);
                break;

            case SERVER_OFF:
                throw new UnsupportedOperationException();
        }
    }

    private void checkClientPoolConnections() {

        Message message = new Message();
        message.setType(MessageType.CHECK_CONNECTION);

        clientsPool.notifyClientHandlers(message);
    }

    private void notifyPoolAboutNewClient() {

        Message message = new Message();
        message.setType(MessageType.LOGIN);
        message.setLogin(clientName);

        clientsPool.notifyClientHandlers(message);
    }

    private void sendRejectMessage() {

        Message message = new Message();
        message.setType(MessageType.REJECT);
        message.setLogin(clientName);
        sendMessage(message);
    }

    private List<Message> readMessage(InputStream inputStream) throws IOException, JAXBException {

        List<Message> messages = new ArrayList<>();
        String inputString = readString(inputStream);

        Pattern pattern = Pattern.compile(PATTERN_STRING);
        Matcher matcher = pattern.matcher(inputString);

        while(matcher.find()) {
            StringReader reader = new StringReader(inputString.substring(matcher.start(), matcher.end()));
            messages.add((Message) unmarshaller.unmarshal(reader));
        }

        return messages;
    }

    private String readString(InputStream inputStream) throws IOException {

        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        int length = bufferedInputStream.available();
        byte[] buffer = new byte[length];
        bufferedInputStream.read(buffer, 0, length);

        return new String(buffer);
    }
}
