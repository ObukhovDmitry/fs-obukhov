package ru.cft.server;

import ru.cft.common.message.Message;

import java.util.Set;

/**
 * ClientPoolObservable - пул клаентов является наблюдаемым для каждого клиента
 */
interface ClientPoolObservable {

    /**
     * addClientHandler добавление нового клиента
     * @param name имя клиента
     * @param listener контроллер клиента
     * @return true/false - если listener добавлен/не добавлен
     */
    boolean addClientHandler(String name, ClientListener listener);

    /**
     * delete удаление клиента
     * @param name имя клиента
     */
    void delete(String name);

    /**
     * notifyClientHandlers оповещение клиентов
     * @param message новое сообщение
     */
    void notifyClientHandlers(Message message);

    /**`
     * getClientsNames возвращает список имен пользователей
     * @return список имен пользователей
     */
    Set<String> getClientsNames();
}
