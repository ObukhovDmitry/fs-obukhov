package ru.cft.server;

public class Main {

    public static void main(String[] args) {

        ServerCommandQueue serverCommandsQueue = new ServerCommandQueue();
        ServerHandler serverHandler = new ServerHandler(serverCommandsQueue);
        serverHandler.run();
    }
}
