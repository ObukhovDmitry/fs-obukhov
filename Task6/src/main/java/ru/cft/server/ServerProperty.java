package ru.cft.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.util.Properties;

class ServerProperty {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerProperty.class);
    private static final String PROPERTY_FILE = "server.properties";
    private static final String PORT_KEY = "port";
    private static final int DEFAULT_PORT = 4321;

    int getPort() {

        int port = DEFAULT_PORT;
        Properties properties = new Properties();

        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream(PROPERTY_FILE)) {

            properties.load(inputStream);
            port = Integer.parseInt(properties.getProperty(PORT_KEY));

        } catch (Exception e) {
            LOGGER.error("Oops! Property can't be loaded.");
        }

        return port;
    }
}
