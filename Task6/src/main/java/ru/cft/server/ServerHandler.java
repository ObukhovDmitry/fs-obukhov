package ru.cft.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.common.command.IQueryQueue;

import java.util.Optional;

class ServerHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerHandler.class);
    private static final long THREAD_SLEEP_MILLS = 100;

    private Server server;
    private Thread serverThread;
    private IQueryQueue<ServerCommand> serverCommandsQueue;
    private boolean isStopped;

    ServerHandler(IQueryQueue<ServerCommand> serverCommandsQueue) {

        this.serverCommandsQueue = serverCommandsQueue;

        ServerProperty serverProperty = new ServerProperty();
        server = new Server(serverProperty.getPort());
        serverThread = new Thread();
    }

    void run() {

        while (!isStopped) {

            if (serverCommandsQueue.hasNext()) {
                Optional<ServerCommand> query = serverCommandsQueue.next();
                query.ifPresent(this::handleCommand);
            }

            try {
                Thread.sleep(THREAD_SLEEP_MILLS);
            } catch (InterruptedException e) {
                LOGGER.error("Thread of server handler is interrupted.");
            }
        }
    }

    private void handleCommand(ServerCommand query) {

        switch (query) {

            case START:
                startServer();
                break;

            case STOP:
                stopServer();
                break;

            case STATUS:
                showStatus();
                break;

            case HELP:
                help();
                break;

            case EXIT:
                exit();
                break;
        }
    }

    private void startServer() {

        if (server.isStarted()) {
            LOGGER.info("Server is already started.");
        } else {
            serverThread = new Thread(server);
            serverThread.start();
        }
    }

    private void stopServer() {

        if (server.isStarted()) {
            server.end();
        } else {
            LOGGER.info("Server is already stopped.");
        }
    }

    private void showStatus() {

        if (server.isStarted()) {
            LOGGER.info("Server running.");
        } else {
            LOGGER.info("Server is stopped.");
        }
    }

    private void help() {

        StringBuilder stringBuilder = new StringBuilder("Server commands:\n");

        for (ServerCommand command : ServerCommand.values()) {
            stringBuilder.append(command.getDescription());
        }

        LOGGER.info(stringBuilder.toString());
    }

    private void exit() {

        stopServer();
        isStopped = true;
    }
}
