package ru.cft.server;

import ru.cft.common.message.Message;
import ru.cft.common.message.MessageType;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class ClientsPool implements ClientPoolObservable {

    private Map<String, ClientListener> clientListeners;

    ClientsPool() {
        clientListeners = new ConcurrentHashMap<>();
    }

    @Override
    public boolean addClientHandler(String name, ClientListener listener) {

        boolean isUsedName = clientListeners.containsKey(name);

        if (!isUsedName) {
            clientListeners.put(name, listener);
        }

        return !isUsedName;
    }

    @Override
    public void delete(String name) {
        clientListeners.remove(name);
    }

    @Override
    synchronized public void notifyClientHandlers(Message message) {
        clientListeners.values().forEach(clientListener -> clientListener.sendMessage(message));
    }

    @Override
    public Set<String> getClientsNames() {
        return clientListeners.keySet();
    }

    void notifyEnd() {

        Message message = new Message();
        message.setType(MessageType.SERVER_OFF);
        notifyClientHandlers(message);
        clientListeners.values().forEach(ClientListener::closeConnection);
    }
}
