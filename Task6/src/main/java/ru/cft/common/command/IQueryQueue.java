package ru.cft.common.command;

import java.util.Optional;

/**
 * IQueryQueue - интерфейс, описывающий очередь команд
 */
public interface IQueryQueue<T extends IQuery> {

    boolean hasNext();
    Optional<T> next();
}
