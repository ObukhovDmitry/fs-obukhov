package ru.cft.common.command;

import java.util.Optional;
import java.util.Scanner;

public abstract class ConsoleQueryQueue<T extends IQuery> implements IQueryQueue<T> {

    private static final String CHARSET = "UTF-8";
    private Scanner inputScanner;

    protected ConsoleQueryQueue() {
        inputScanner = new Scanner(System.in, CHARSET);
    }

    @Override
    public boolean hasNext() {
        return inputScanner.hasNextLine();
    }

    @Override
    public Optional<T> next() {

        String line = inputScanner.nextLine();
        return Optional.ofNullable(getQueryByString(line));
    }

    protected abstract T getQueryByString(String line);
}
