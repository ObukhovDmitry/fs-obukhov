package ru.cft.common.message;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

/**
 * MessageType - перечисление типов сообщений
 */
@XmlType(name = "messageEnum")
@XmlEnum
public enum MessageType {

    /**
     * LOGIN - в сообщении содержится логин клиента
     */
    LOGIN,

    /**
     * REJECT - сообщение содержит отказ (нужно для оповещения клиента об уже используемом имени)
     */
    REJECT,

    /**
     * CHECK_CONNECTION - сообщение содержит отказ (нужно для оповещения клиента об уже используемом имени)
     */
    CHECK_CONNECTION,

    /**
     * TEXT - сообщение содрежит текст
     */
    TEXT,

    /**
     * USER_LIST - запрос клиента о получении списка пользователей
     */
    USER_LIST,

    /**
     * CLIENT_OFF - клиент отключается
     */
    CLIENT_OFF,

    /**
     * SERVER_OFF - сервер выключается
     */
    SERVER_OFF
}
