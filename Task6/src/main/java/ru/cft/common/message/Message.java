package ru.cft.common.message;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlType(propOrder = {"type", "login", "text"})
public class Message {

    private MessageType type;
    private String login;
    private String text;

    public MessageType getType() {
        return type;
    }

    @XmlElement(required = true)
    public void setType(MessageType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    @XmlElement
    public void setText(String text) {
        this.text = text;
    }

    public String getLogin() {
        return login;
    }

    @XmlElement
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String toString() {
        return String.format("Message{type=%s, login=%s, text=%s}", type, login, text);
    }
}
