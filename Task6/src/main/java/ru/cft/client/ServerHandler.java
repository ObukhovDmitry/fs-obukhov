package ru.cft.client;

import com.sun.org.apache.bcel.internal.generic.CHECKCAST;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.cft.common.message.Message;
import ru.cft.common.message.MessageType;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.MarshalException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

/**
 * ServerHandler - обеспечивет обратную связь с сервером
 */
class ServerHandler implements IServerHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerHandler.class);
    private static final String FILE_SCHEMA = "message.xsd";

    private static final  int NUMBER_OF_RECONNECT_ATTEMPTS = 30;
    private static final  long DELAY_BETWEEN_RECONNECTION = 1000;

    private Marshaller marshaller;

    private ServerListener serverListener;
    private Socket socket;
    private URL url;
    private String clientName;
    private ConnectionState state = ConnectionState.DISCONNECTED;
    private String exceptionMessage;

    ServerHandler() {

        try {
            JAXBContext context = JAXBContext.newInstance(Message.class);
            marshaller = context.createMarshaller();

            ClassLoader classLoader = getClass().getClassLoader();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(classLoader.getResource(FILE_SCHEMA).getFile()));
            marshaller.setSchema(schema);

        } catch (JAXBException | SAXException e) {
            LOGGER.error("Initialization error. ", e);
        }
    }

    void setServerListener(ServerListener serverListener) {
        this.serverListener = serverListener;
    }

    @Override
    public ConnectionState getConnectionState() {
        updateConnectionState();
        return state;
    }

    @Override
    public boolean connect(URL url) {

        boolean isDid = false;

        switch (state) {

            case CONNECTED:
                exceptionMessage = String.format("You ara already connected to %s.", url);
                break;

            case RECONNECT:
                isDid = joinWhenNotConnected(this.url);
                break;

            case DISCONNECTED:
                isDid = joinWhenNotConnected(url);
                break;
        }

        return  isDid;
    }

    @Override
    public void disconnect() {

        try {
            socket.close();
            url = null;
            state = ConnectionState.DISCONNECTED;

        } catch (IOException e) {
            LOGGER.error("Socket closing error.");
        }
    }

    @Override
    public boolean send(Message message) {

        boolean isDid = false;

        switch (state) {

            case CONNECTED:
                isDid = sendWhenConnected(message);
                if (!isDid && reconnect()) {
                    isDid = sendLogin() && sendWhenConnected(message);
                }
                break;

            case RECONNECT:
                if (reconnect()) {
                    isDid = sendLogin() && sendWhenConnected(message);
                }
                break;

            case DISCONNECTED:
                exceptionMessage = "There is no connection.";
                break;
        }

        return isDid;
    }

    @Override
    public String getExceptionMessage() {
        return exceptionMessage;
    }

    @Override
    public URL getUrl() {
        return url;
    }

    @Override
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    private void updateConnectionState() {

        switch (state) {

            case CONNECTED:
                if (!checkConnect()) {
                    state = ConnectionState.RECONNECT;
                }
                break;

            case RECONNECT:
                if (joinWhenNotConnected(this.url) && sendLogin()) {
                    state = ConnectionState.CONNECTED;
                }
                break;

            case DISCONNECTED:
                break;
        }
    }

    private boolean checkConnect() {

        boolean hasConnect = false;

        Message message = new Message();
        message.setType(MessageType.CHECK_CONNECTION);

        try {
            marshaller.marshal(message, socket.getOutputStream());

            hasConnect = true;
            LOGGER.debug("{} message sent.", message.getType());

        } catch (IOException | JAXBException e) {
            LOGGER.debug("Connection message can't be sent.", message.getType());
        }

        return hasConnect;
    }

    private boolean reconnect() {

        boolean isDid = false;
        URL oldUrl = url;

        disconnect();
        LOGGER.info("Server is not responding.");

        int currentAttemptOfReconnect = 0;
        while (currentAttemptOfReconnect++ < NUMBER_OF_RECONNECT_ATTEMPTS &&
                !isDid) {

            LOGGER.debug("Attempt to reconnect #{}", currentAttemptOfReconnect);
            isDid = joinWhenNotConnected(oldUrl);

            try {
                Thread.sleep(DELAY_BETWEEN_RECONNECTION);
            } catch (InterruptedException e) {
                LOGGER.error("Can't reconnect, the thread was interrupted. ", e);
            }
        }

        return isDid;
    }

    private boolean joinWhenNotConnected(URL url) {

        boolean isDid = false;

        try {
            socket = new Socket(url.getHost(), url.getPort());
            LOGGER.debug("Client socket open.");

            serverListener.setSocket(socket);
            new Thread(serverListener).start();

            this.url = url;
            state = ConnectionState.CONNECTED;
            isDid = true;

        } catch (IOException e) {
            exceptionMessage = String.format("%s is not response.", url);
        }

        return isDid;
    }

    private boolean sendWhenConnected(Message message) {

        boolean isDid = false;

        try {
            marshaller.marshal(message, socket.getOutputStream());

            isDid = true;
            LOGGER.debug("{} message sent.", message.getType());

        } catch (IOException e) {
            exceptionMessage = "Socket error.";
        } catch (JAXBException e) {
            exceptionMessage = "JAXB error.";
        }

        return isDid;
    }

    private boolean sendLogin() {

        Message login = new Message();
        login.setType(MessageType.LOGIN);
        login.setLogin(clientName);

        return sendWhenConnected(login);
    }
}
