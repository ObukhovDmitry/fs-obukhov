package ru.cft.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.common.message.Message;
import ru.cft.common.message.MessageType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.OutputStream;

class DecisionMaker implements IClientQueryHandler, IServerMessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecisionMaker.class);

    private IView view;
    private IServerHandler server;

    DecisionMaker(IView view, IServerHandler server) {

        this.view = view;
        this.server = server;
    }

    @Override
    public void handleServerMessage(Message message) {

        switch (message.getType()) {

            case LOGIN:
                view.handle(String.format("%s connected.", message.getLogin()));
                break;

            case REJECT:
                view.handle(String.format("Name %s is used.", message.getLogin()));
                server.disconnect();
                break;

            case TEXT:
                view.handle(String.format("%s: %s", message.getLogin(), message.getText()));
                break;

            case USER_LIST:
                view.handle(String.format("User list: %s", message.getText()));
                break;

            case CLIENT_OFF:
                view.handle(String.format("%s disconnected.", message.getLogin()));
                break;

            case SERVER_OFF:
                view.handle("Server shut down.");
                server.disconnect();
                break;
        }
    }

    @Override
    public void handleClientQuery(ClientQuery query) {

        if (query.getCommand() != null) {
            handleClientQueryCommand(query);

        } else {
            handleClientQueryMessage(query.getMessage());
        }
    }

    private void handleClientQueryCommand(ClientQuery query) {

        switch (query.getCommand()) {

            case JOIN:
                joinServer(query);
                break;

            case LEAVE:
                leaveServer(query);
                break;

            case USER_LIST:
                receiveUserList(query);
                break;

            case STATUS:
                showStatus();
                break;

            case HELP:
                help();
                break;

            case EXIT:
                exit();
                break;
        }
    }

    private void handleClientQueryMessage(Message message) {

        if (!server.send(message)) {
            view.handle(String.format("Message can't be sent. Reason: %s", server.getExceptionMessage()));
        }
    }

    private void joinServer(ClientQuery query) {

        if (server.connect(query.getUrl())) {
            server.setClientName(query.getMessage().getLogin());
            server.send(query.getMessage());
        } else {
            view.handle(String.format("Can't connect to the server. Reason: %s", server.getExceptionMessage()));
        }
    }

    private void leaveServer(ClientQuery query) {

        if (server.send(query.getMessage())) {
            server.disconnect();
        } else {
            view.handle(String.format("Can't disconnect from the server. Reason: %s", server.getExceptionMessage()));
        }
    }

    private void receiveUserList(ClientQuery query) {

        if (!server.send(query.getMessage())) {
            view.handle(String.format("Can't receive user list. Reason: %s", server.getExceptionMessage()));
        }
    }

    private void showStatus() {

        switch (server.getConnectionState()) {

            case CONNECTED:
                view.handle(String.format("You are connected to %s", server.getUrl()));
                break;

            case RECONNECT:
                view.handle(String.format("You are connect to %s, but server is not responding.", server.getUrl()));
                break;

            case DISCONNECTED:
                view.handle("You are not connected to any server.");
                break;
        }
    }

    private void help() {

        StringBuilder stringBuilder = new StringBuilder("Client commands:\n");

        for (ClientCommand command : ClientCommand.values()) {
            stringBuilder.append(command.getDescription());
        }

        view.handle(stringBuilder.toString());
    }

    private void exit() {

        Message message = new Message();
        message.setType(MessageType.CLIENT_OFF);

        ClientQuery query = new ClientQuery();
        query.setMessage(message);

        if (server.send(query.getMessage())) {
            server.disconnect();
        }
    }
}
