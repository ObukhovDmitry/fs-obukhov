package ru.cft.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.common.command.IQueryQueue;

import java.util.Optional;

/**
 * QueryListener - слушатель запросов пользователя
 */
class QueryListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryListener.class);
    private static final long THREAD_SLEEP_MILLS = 100;

    private IClientQueryHandler queryHandler;
    private IQueryQueue<ClientQuery> clientQueryQueue;
    private boolean isEnd;

    QueryListener(IClientQueryHandler queryHandler, IQueryQueue<ClientQuery> clientQueryQueue) {

        this.queryHandler = queryHandler;
        this.clientQueryQueue = clientQueryQueue;
    }

    void run() {

        while (!isEnd) {

            if (clientQueryQueue.hasNext()) {
                Optional<ClientQuery> query = clientQueryQueue.next();
                query.ifPresent(this::checkEnd);
                query.ifPresent(queryHandler::handleClientQuery);

            } else {
                try {
                    Thread.sleep(THREAD_SLEEP_MILLS);
                } catch (InterruptedException e) {
                    LOGGER.error("Thread of server handler is interrupted.");
                }
            }
        }
    }

    private void checkEnd(ClientQuery query) {
        isEnd = query.getCommand() != null && query.getCommand() == ClientCommand.EXIT;
    }
}
