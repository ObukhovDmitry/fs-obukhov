package ru.cft.client;

/**
 * ConnectionState - перечисление возможных состояний соединения
 */
enum ConnectionState {

    /**
     * CONNECTED - соединение установлено
     */
    CONNECTED,

    /**
     * RECONNECT - попытка пересоединиться
     */
    RECONNECT,

    /**
     * DISCONNECTED - соединения нет
     */
    DISCONNECTED
}
