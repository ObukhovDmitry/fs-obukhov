package ru.cft.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ConsoleView - предоставление сообщений пользователю через консоль
 */
class ConsoleView implements IView {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleView.class);

    @Override
    public void handle(String string) {
        LOGGER.info(string);
    }
}
