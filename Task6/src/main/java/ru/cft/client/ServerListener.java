package ru.cft.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;
import ru.cft.common.message.Message;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ServerListener - слушатель запросов с сервера
 */
class ServerListener implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServerListener.class);
    private static final String FILE_SCHEMA = "message.xsd";
    private static final String PATTERN_STRING = "<\\?xml(.*?)</message>";
    private static final long THREAD_SLEEP_MILLS = 100;
    private static final int BUFFER_SIZE = 1024;

    private Unmarshaller unmarshaller;

    private IServerMessageHandler messageHandler;
    private Socket socket;

    ServerListener(IServerMessageHandler messageHandler) {

        this.messageHandler = messageHandler;

        try {
            JAXBContext context = JAXBContext.newInstance(Message.class);
            unmarshaller = context.createUnmarshaller();

            ClassLoader classLoader = getClass().getClassLoader();
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(classLoader.getResource(FILE_SCHEMA).getFile()));
            unmarshaller.setSchema(schema);

        } catch (JAXBException | SAXException e) {
            LOGGER.error("Initialization error. ", e);
        }

    }

    void setSocket(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {

        LOGGER.debug("Server listener running.");

        while (!socket.isClosed()) {

            try {
                if (socket.getInputStream().available() > 0) {

                    List<Message> messages = readMessages(socket.getInputStream());
                    messages.forEach(messageHandler::handleServerMessage);

                } else {
                    Thread.sleep(THREAD_SLEEP_MILLS);
                }
            } catch (IOException e) {
                LOGGER.error("Socket closed.");
            } catch (JAXBException e) {
                LOGGER.error("JAXB failed.");
            } catch (InterruptedException e) {
                LOGGER.error("DecisionMaker listener thread interrupted.");
            }
        }
    }

    private List<Message> readMessages(InputStream inputStream) throws IOException, JAXBException {

        List<Message> messages = new ArrayList<>();
        String inputString = readString(inputStream);

        Pattern pattern = Pattern.compile(PATTERN_STRING);
        Matcher matcher = pattern.matcher(inputString);

        while(matcher.find()) {
            StringReader reader = new StringReader(inputString.substring(matcher.start(), matcher.end()));
            messages.add((Message) unmarshaller.unmarshal(reader));
        }

        return messages;
    }

    private String readString(InputStream inputStream) throws IOException {

        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);

        int length = bufferedInputStream.available();
        byte[] buffer = new byte[length];
        bufferedInputStream.read(buffer, 0, length);

        return new String(buffer);
    }
}
