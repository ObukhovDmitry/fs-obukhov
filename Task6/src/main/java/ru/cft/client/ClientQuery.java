package ru.cft.client;

import ru.cft.common.command.IQuery;
import ru.cft.common.message.Message;

/**
 * ClientQuery - структура данных для хранения запроса
 */
class ClientQuery implements IQuery {

    private ClientCommand command;
    private Message message;
    private URL url;

    ClientCommand getCommand() {
        return command;
    }

    void setCommand(ClientCommand command) {
        this.command = command;
    }

    Message getMessage() {
        return message;
    }

    void setMessage(Message message) {
        this.message = message;
    }

    URL getUrl() {
        return url;
    }

    void setUrl(URL url) {
        this.url = url;
    }
}
