package ru.cft.client;

/**
 * IClientQueryHandler - класс, который реализует этот интерфейс, может обрабатывать запросы клиента
 */
interface IClientQueryHandler {

    /**
     * Обработать запрос
     * @param query запрос, которое нужно обработать
     */
    void handleClientQuery(ClientQuery query);
}
