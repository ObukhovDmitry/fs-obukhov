package ru.cft.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.common.command.ConsoleQueryQueue;
import ru.cft.common.message.Message;
import ru.cft.common.message.MessageType;

/**
 * ClientQueryQueue - очередь запросов от клиента
 */
class ClientQueryQueue extends ConsoleQueryQueue<ClientQuery> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientQueryQueue.class);

    @Override
    protected ClientQuery getQueryByString(String queryText) {
        return isCommand(queryText) ? processCommand(queryText) : processMessage(queryText);
    }

    private boolean isCommand(String queryText) {
        return queryText.length() != 0 && queryText.charAt(0) == '/';
    }

    private ClientQuery processMessage(String queryText) {

        ClientQuery query = new ClientQuery();

        Message message = new Message();
        message.setType(MessageType.TEXT);
        message.setText(queryText);
        query.setMessage(message);

        LOGGER.debug("Query: {command = {}, message = {}, url = {}}", query.getCommand(), query.getMessage(), query.getUrl());

        return query;
    }
    
    private ClientQuery processCommand(String queryText) {

        ClientQuery query = new ClientQuery();

        String[] separatedQuery = queryText.split(" ");
        ClientCommand command = ClientCommand.getByConsoleName(separatedQuery[0]);
        query.setCommand(command);

        if (command == ClientCommand.JOIN) {
            try {
                URL url = new URL(separatedQuery[1], Integer.parseInt(separatedQuery[2]));
                query.setUrl(url);

                Message message = new Message();
                message.setType(MessageType.LOGIN);
                message.setLogin(separatedQuery[3]);
                query.setMessage(message);

            } catch (Exception ignore) {
                query = null;
            }

        } else if (command == ClientCommand.LEAVE) {
            Message message = new Message();
            message.setType(MessageType.CLIENT_OFF);
            query.setMessage(message);

        } else if (command == ClientCommand.USER_LIST) {
            Message message = new Message();
            message.setType(MessageType.USER_LIST);
            query.setMessage(message);
        } else if (command == null) {
            query = null;
        }

        if (query == null) {
            LOGGER.info("Undefined command: {}", queryText);
        } else {
            LOGGER.debug("Query: {command = {}, message = {}, url = {}}", query.getCommand(), query.getMessage(), query.getUrl());
        }

        return query;
    }
}
