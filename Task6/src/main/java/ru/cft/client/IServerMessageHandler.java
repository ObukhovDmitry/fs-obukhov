package ru.cft.client;

import ru.cft.common.message.Message;

/**
 * IServerMessageHandler - класс, который реализует этот интерфейс, может обрабатывать сообщения сервера
 */
interface IServerMessageHandler {

    /**
     * Опбработать запрос
     * @param message сообщение, которое нужно обработать
     */
    void handleServerMessage(Message message);
}
