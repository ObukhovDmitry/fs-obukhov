package ru.cft.client;

/**
 * IView - реализизующий класс, должен реализовать способ вывода информации пользователю
 */
interface IView {

    /**
     * handle - выводит строку пользователю
     * @param string строка
     */
    void handle(String string);
}
