package ru.cft.client;

/**
 * URL - структура данных для хранения хоста и порта сервера
 */
class URL {

    private String host;
    private int port;

    URL(String host, int port) {

        this.host = host;
        this.port = port;
    }

    String getHost() {
        return host;
    }

    int getPort() {
        return port;
    }

    @Override
    public String toString() {
        return host + ':' + port;
    }
}
