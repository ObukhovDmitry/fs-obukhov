package ru.cft.client;

import ru.cft.common.message.Message;

/**
 * IServerHandler - реалилзующий класс должен определить методы обеспечивающие связь и общение с сервером
 */
interface IServerHandler {

    /**
     * Получить состояние соединения с сервером
     */
    ConnectionState getConnectionState();

    /**
     * Соединение с сервером по заданному url
     * @param url   url, который содержит host и port для соединения
     * @return  true - если соединение успешно,
     *          false - иначе.
     */
    boolean connect(URL url);

    /**
     * Разрыв соединения с сервером
     */
    void disconnect();

    /**
     * Отправить сообщение на сервер
     * @param message сообщение
     * @return  true - если сообщение отправлено,
     *          false - иначе.
     */
    boolean send(Message message);

    /**
     * Получить сообщение о ходе выполнения последней команды
     */
    String getExceptionMessage();

    /**
     * Получить URL сервера
     * @return  URL сервера, если соединение есть или идет восстановление соединения,
     *          null иначе
     */
    URL getUrl();

    /**
     * Установить имя клиента, которое является его уникальным идентификатором
     * @param clientName имя клиента
     */
    void setClientName(String clientName);
}
