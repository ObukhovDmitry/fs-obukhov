package ru.cft.client;

import java.util.HashMap;
import java.util.Map;

/**
 * ServerCommand - перечисление команд, которые может выполнять клиент
 */
enum ClientCommand {

    /**
     * JOIN - соединение с сервером
     */
    JOIN ("/join", "\t/join [host] [port] [login]\tConnect to server host:port with the login\n"),

    /**
     * LEAVE - прервать соединение с сервером
     */
    LEAVE ("/leave", "\t/leave\tDisconnect from the server\n"),

    /**
     * STATUS - прервать текущее состояние (присоеденины ли к серверу или нет)
     */
    STATUS ("/status", "\t/status\tCheck client connection\n"),

    /**
     * USER_LIST - получить список польлзователей чата
     */
    USER_LIST ("/userlist", "\t/userlist\tGet a list of chat users\n"),

    /**
     * HELP - список команд и их краткое описание
     */
    HELP ("/help", "\t/help\tCommand list\n"),

    /**
     * CLIENT_OFF - прервать соединение и выход из программы
     */
    EXIT ("/exit","\t/exit\tDisconnect from the server and exit");

    /**
     * command - имя команды в консоли
     */
    private String consoleName;

    /**
     * description - описание команды
     */
    private String description;

    /**
     * commandByConsoleName - мапа для получения команды по её названию в консоли
     */
    private static final Map<String, ClientCommand> commandByConsoleName = new HashMap<>();
    static {
        for (ClientCommand command : ClientCommand.values()) {
            commandByConsoleName.put(command.consoleName, command);
        }
    }

    /**
     * Конструктор
     * @param consoleName имя команды в консоли
     * @param description описание команды
     */
    ClientCommand(String consoleName, String description) {

        this.consoleName = consoleName;
        this.description = description;
    }

    /**
     * getDescription - возвращает описание команды
     * @return строковое описание команды
     */
    String getDescription() {
        return description;
    }

    /**
     * Возвращает по названию команды в консоле саму команду из перечисления
     * @param name название команды в консоли
     * @return команда
     */
    static ClientCommand getByConsoleName(String name) {
        return commandByConsoleName.get(name);
    }
}
