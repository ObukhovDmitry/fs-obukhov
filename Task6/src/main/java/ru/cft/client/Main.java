package ru.cft.client;

public class Main {

    public static void main(String[] args) {

        ConsoleView consoleView = new ConsoleView();
        ServerHandler serverHandler = new ServerHandler();
        DecisionMaker decisionMaker = new DecisionMaker(consoleView, serverHandler);

        ServerListener serverListener = new ServerListener(decisionMaker);
        serverHandler.setServerListener(serverListener);

        ClientQueryQueue clientQueryQueue = new ClientQueryQueue();
        QueryListener queryListener = new QueryListener(decisionMaker, clientQueryQueue);

        queryListener.run();
    }
}
