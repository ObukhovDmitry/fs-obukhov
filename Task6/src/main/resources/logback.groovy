import ch.qos.logback.classic.filter.ThresholdFilter

appender("RELEASE", ConsoleAppender) {
    target = "System.out"
    filter(ThresholdFilter) {
        level = INFO
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %msg%n"
    }
}

appender("DEBUG", ConsoleAppender) {
    target = "System.out"
    filter(ThresholdFilter) {
        level = DEBUG
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %-5level %logger - %msg%n"
    }
}

root(DEBUG, ["DEBUG"])