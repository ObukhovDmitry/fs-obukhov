package ru.cft;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public class TablePrinter<T> {

    private List<T> rows;
    private List<T> cols;
    private BinaryOperator<T> operator;
    private String formatFirstColumn;
    private String formatTable;
    private String separator;

    public TablePrinter(
            List<T> rows,
            List<T> cols,
            BinaryOperator<T> operator) {

        this.rows = rows;
        this.cols = cols;
        this.operator = operator;

        initFormats();
    }

    public void print() {

        printHead();
        printSeparator();
        for (int i = 0; i < rows.size(); i++) {
            printLine(i);
            printSeparator();
        }
    }

    private void printHead() {

        printFirstColumn(null);
        printLine(i -> cols.get(i));
    }

    private void printSeparator() {

        System.out.printf(separator);
    }

    private void printLine(int row) {

        printFirstColumn(rows.get(row));
        printLine(i -> operator.apply(cols.get(i), rows.get(row)));
    }

    private void printFirstColumn(T value) {

        String valueString = value == null ? "" : value.toString();
        System.out.printf(formatFirstColumn, valueString);
    }

    private void printLine(Function<Integer, T> function) {

        for (int i = 0; i < cols.size(); i++) {
            System.out.printf(formatTable, function.apply(i));
        }
    }

    private void initFormats() {

        int charsFirstColumn = getMaximumOfFunction(i -> getNumberOfChars(rows.get(i)), rows.size());
        formatFirstColumn = "%" + charsFirstColumn + "s";

        int numberOfChars = getMaximumOfFunction(this::getCharsForLine, rows.size());
        formatTable = "|%" + numberOfChars + "s";

        initSeparator(charsFirstColumn, numberOfChars);
    }

    private void initSeparator(int charsFirstColumn, int numberOfChars) {

        StringBuilder subLine = new StringBuilder();
        subLine.append('+');
        for (int j = 0; j < numberOfChars; j++) {
            subLine.append('-');
        }

        StringBuilder line = new StringBuilder();
        line.append('\n');
        for (int i = 0; i < charsFirstColumn; i++) {
            line.append('-');
        }
        for (int i = 0; i < cols.size(); i++) {
            line.append(subLine);
        }
        line.append('\n');

        separator = line.toString();
    }

    private int getCharsForLine(int row) {

        return getMaximumOfFunction(i -> getNumberOfChars(
                operator.apply(rows.get(row), cols.get(i))),
                cols.size());
    }

    private int getMaximumOfFunction(Function<Integer, Integer> function, int numberOfElements) {

        int max = 0;

        for (int i = 0; i < numberOfElements; i++) {
            max = Math.max(
                    max,
                    function.apply(i));
        }

        return max;
    }

    private int getNumberOfChars(T element) {

        return element.toString().length();
    }
}
