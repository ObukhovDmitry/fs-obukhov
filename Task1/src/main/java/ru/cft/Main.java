package ru.cft;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static final int MIN_TABLE_SIZE = 1;
    private static final int MAX_TABLE_SIZE = 32;

    public static void main(String[] args) {

        List<Integer> list = getListInteger();
        TablePrinter<Integer> tablePrinter = new TablePrinter<>(list, list, (x, y) -> x * y);
        tablePrinter.print();
    }

    private static List<Integer> getListInteger() {

        int n = readTableSize();

        List<Integer> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            list.add(i + 1);
        }

        return list;
    }

    private static int readTableSize() {

        String inputString = null;
        int n = 0;

        while (true) {

            System.out.println("Введите размер таблицы: ");

            try {
                Scanner in = new Scanner(System.in);
                inputString = in.nextLine();
                n = Integer.parseInt(inputString);

            } catch (NumberFormatException ignore) {}

            if (n >= MIN_TABLE_SIZE && n <= MAX_TABLE_SIZE) {
                break;
            }

            System.out.println(inputString + " не является целым числом в диапазоне от 1 до 32!");
        }

        return n;
    }
}
